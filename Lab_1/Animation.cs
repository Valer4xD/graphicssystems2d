﻿using System;

namespace Lab_1 {
    [Serializable]
    public class Animation
    {
		public double move_delta_X = 0.0,
		              move_delta_Y = 0.0,
		              zoom_delta_X = 1.0,
		              zoom_delta_Y = 1.0,
		              turn_delta   = 0.0;

	    public double axis_X_zoom = 0.0,
	                  axis_Y_zoom = 0.0,
	                  axis_X_turn = 0.0,
	                  axis_Y_turn = 0.0;

	    public int move_priority = -1, // Диапозон обрабатываемых значений: 1-3
		           zoom_priority = -1, // Диапозон обрабатываемых значений: 1-3
		           turn_priority = -1; // Диапозон обрабатываемых значений: 1-3

        public int time_out   =  0,
                   iterations =  0;

		public bool Single_Center()
		{
			if(axis_X_zoom == axis_X_turn
			&& axis_Y_zoom == axis_Y_turn)		return true;
			return false;
		}

        public Animation Copy(Animation animation)
		{
			move_delta_X = animation.move_delta_X;
			move_delta_Y = animation.move_delta_Y;
			zoom_delta_X = animation.zoom_delta_X;
			zoom_delta_Y = animation.zoom_delta_Y;
			turn_delta	 = animation.turn_delta;

			axis_X_zoom = animation.axis_X_zoom;
			axis_Y_zoom = animation.axis_Y_zoom;
			axis_X_turn = animation.axis_X_turn;
			axis_Y_turn = animation.axis_Y_turn;

			move_priority = animation.move_priority;
			zoom_priority = animation.zoom_priority;
			turn_priority = animation.turn_priority;

			time_out   = animation.time_out;
			iterations = animation.iterations;

            return this;
		}
    }
}