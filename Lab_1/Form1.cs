﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Lab_1
{
    public partial class Form1 : Form
    {
        public static object locker = new object();

        List<Primitive> all_figures = new List<Primitive>();

        int screen_width  = 0,
            screen_height = 0,
            diff_form_screen_X = 0,
            diff_form_screen_Y = 0;

        Graphics g;
        Pen      draw_pen   = new Pen(Color.Black,   1);
        Pen      select_pen = new Pen(Color.Magenta, 1);
        Bitmap   bmp;

        List<Point> ArPoints    = new List<Point>();    // Массив точек

        bool g_test_paint = false;

        bool mouse_drag = false;
        int mouse_X = 0,
            mouse_Y = 0,
            num_selected_figures = 0;

        bool wheel_mode = false;

        const int LINE              = 1,
                  CURVE             = 2,
                  POLYGON           = 3,
                  TRIANGLE          = 4,
                  ARROW             = 5,
                  CROSSHAIR         = 6,
                  VERTIVAL_STRAIGHT = 7,
                  RANDOM_STRAIGHT   = 8,
                  CIRCLE            = 9;
        int type_primitive = POLYGON;

        bool creating = false;

        bool context_menu_block = false;
        bool choice_figure = false;
        int choice_figure_id = 0;

        int type_transformations = -1;
        const int TURN_30_CLOCK_WISE = 1,
                  TURN_30_COUNTER_CW = 2,
                  MIRROR_VERTICAL    = 3,
                  MIRROR_RANDOM      = 4;
        bool relative_figure_create = false;

        private ToolStripRadioButtonMenuItem line_segment_ToolStripMenuItem         =
            new ToolStripRadioButtonMenuItem();
        private ToolStripRadioButtonMenuItem bezier_curve_ToolStripMenuItem         =
            new ToolStripRadioButtonMenuItem();
        private ToolStripRadioButtonMenuItem polygon_ToolStripMenuItem              =
            new ToolStripRadioButtonMenuItem();
        private ToolStripRadioButtonMenuItem triangle_isosceles_ToolStripMenuItem   =
            new ToolStripRadioButtonMenuItem();
        private ToolStripRadioButtonMenuItem arrow_ToolStripMenuItem                =
            new ToolStripRadioButtonMenuItem();

        public Form1()
        {
            InitializeComponent();

            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            // this.DoubleBuffered = true; // EDIT
            // this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true); // EDIT

            g_test_paint = checkBox1.Checked;

            switch_activity();

            line_segment_ToolStripMenuItem.         Text = "Отрезок";
            bezier_curve_ToolStripMenuItem.         Text = "Кривая Безье";
            polygon_ToolStripMenuItem.              Text = "Многоугольник";
            triangle_isosceles_ToolStripMenuItem.   Text = "Равнобедренный треугольник";
            arrow_ToolStripMenuItem.                Text = "Стрелка";

            type_primitive_ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                line_segment_ToolStripMenuItem,
                bezier_curve_ToolStripMenuItem,
                polygon_ToolStripMenuItem,
                triangle_isosceles_ToolStripMenuItem,
                arrow_ToolStripMenuItem });

            polygon_ToolStripMenuItem.Checked = true;

            line_segment_ToolStripMenuItem.Click       += new System.EventHandler(line_segment_ToolStripMenuItem_Click);
            bezier_curve_ToolStripMenuItem.Click       += new System.EventHandler(bezier_curve_ToolStripMenuItem_Click);
            polygon_ToolStripMenuItem.Click            += new System.EventHandler(polygon_ToolStripMenuItem_Click);
            triangle_isosceles_ToolStripMenuItem.Click += new System.EventHandler(triangle_isosceles_ToolStripMenuItem_Click);
            arrow_ToolStripMenuItem.Click              += new System.EventHandler(arrow_ToolStripMenuItem_Click);

            screen_width  = pbDrawLine.Width;
            screen_height = pbDrawLine.Height;
            diff_form_screen_X = this.Width  - screen_width;
            diff_form_screen_Y = this.Height - screen_height;

            bmp = new Bitmap(screen_width,
                             screen_height);
            g = Graphics.FromImage(bmp);
            pbDrawLine.Image = bmp;

            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 1;
            comboBox3.SelectedIndex = 2;

            comboBox4.SelectedIndex = 0;

            this.MouseWheel += new MouseEventHandler(this_MouseWheel);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lock(locker)    Draw();
        }

        private void Draw()
        {
            g.Clear(Color.White);

            int num_figures = all_figures.Count(),
                border = num_figures - num_selected_figures,
                F = 0;

            for(F = 0; F < num_figures; ++ F)
            {
                if(F >= border && F < num_figures - 1)
                    all_figures[F].Paint_Borders(g, select_pen, screen_width, screen_height);
                all_figures[F].Paint(g, all_figures[F].pen, screen_width, screen_height, g_test_paint);
            }

            if(creating)
                switch(type_primitive)
                {
                    case TRIANGLE:
                    case ARROW:
                    {
                        MyDraw.Line(g, select_pen, screen_height, true, ArPoints[0].X, ArPoints[0].Y,
                                                                        ArPoints[0].X, ArPoints[1].Y, true);
                        MyDraw.Line(g, select_pen, screen_height, true, ArPoints[0].X, ArPoints[0].Y,
                                                                        ArPoints[1].X, ArPoints[0].Y, true);
                        MyDraw.Line(g, select_pen, screen_height, true, ArPoints[1].X, ArPoints[1].Y,
                                                                        ArPoints[1].X, ArPoints[0].Y, true);
                        MyDraw.Line(g, select_pen, screen_height, true, ArPoints[1].X, ArPoints[1].Y,
                                                                        ArPoints[0].X, ArPoints[1].Y, true);
                        break;
                    }
                    case CIRCLE:
                    {
                        MyDraw.Line(g, select_pen, screen_height, true, ArPoints[0], ArPoints[1], true);
                        break;
                    }
                    case CROSSHAIR:
                    {
                        MyDraw.Line(g, select_pen, screen_height, true, ArPoints[0], ArPoints[1], true);

                        Point center = ArPoints[0];
                        double width = (draw_pen.Width > select_pen.Width) ? draw_pen.Width : select_pen.Width,
                               radius = draw_pen.Width / 2.0;
                               // radius = Math.Sqrt(draw_pen.Width * draw_pen.Width / 2.0); // Описанная вокруг квадрата окружность со сторонами = draw_pen.Width
                        MyDraw.Circle(g, select_pen, screen_height, true, center, radius);

                        radius = STO.get_distance(ArPoints[0], ArPoints[1]);
                        radius = Math.Sqrt(radius * radius + draw_pen.Width * draw_pen.Width / 4.0); // Радиус описанной окружности с учётом draw_pen.Width
                        MyDraw.Circle(g, select_pen, screen_height, false, center, radius);

                        break;
                    }
                }
            else
            {
                Point center = new Point();
                double radius = draw_pen.Width / 2.0 + 2.0;

                int num_points = ArPoints.Count();
                for(int C = 0, N = 1; C < num_points; ++ C, ++ N)
                {
                    center = ArPoints[C];

                    MyDraw.Circle(g, draw_pen, screen_height, true, center, radius);

                    if(g_test_paint && num_points > 1)
                        if(N < num_points)
                            MyDraw.Line(g, draw_pen, screen_height, true, ArPoints[C],
                                                                          ArPoints[N], true);
                }

                if(num_selected_figures > 0)
                    all_figures[num_figures - 1].Paint_Borders(g, select_pen, screen_width, screen_height);
            }

            pbDrawLine.Image = bmp;
        }

// Мышь ----------------------------------------------------------------------------------

        // Обработчик вращения колеса мыши
        private void this_MouseWheel(object sender, MouseEventArgs e)
        {
            lock(locker)
            {
                Animation animation = new Animation();
                PointD center = new PointD();

                if(wheel_mode)
                {
                    animation.zoom_priority = 1;
                    if(e.Delta > 0)
                    {
                        animation.zoom_delta_X = 1.1;
                        animation.zoom_delta_Y = 1.1;
                    }
                    else
                    {
                        animation.zoom_delta_X = 0.9;
                        animation.zoom_delta_Y = 0.9;
                    }
                }
                else
                {
                    animation.turn_priority = 1;
                    animation.turn_delta = 5; // Угол поворота в градусах
                    if(e.Delta > 0)     animation.turn_delta *= -1;
                }

                Primitive OBJ = new Primitive();
                int num_figures  = all_figures.Count(),
                    border = num_figures - num_selected_figures;
                for(int F = border, num_polygons, P; F < num_figures; ++ F)
                {
                    OBJ = all_figures[F];
                    if(OBJ.GetType() == typeof(Straight_Random) && wheel_mode)   continue;

                    center = OBJ.Get_Center();
                    animation.axis_X_turn = animation.axis_X_zoom = center.X;
                    animation.axis_Y_turn = animation.axis_Y_zoom = center.Y;

                    num_polygons = OBJ.multi_figure.Count();
                    for(P = 0; P < num_polygons; ++ P)
                        OBJ.Polygon_Transformation(OBJ.multi_figure[P], animation);

                    OBJ.Polygon_Transformation(OBJ.skelet, animation);
                }
            }
        }

        // Обработчик движения мыши
        private void x_mouse_move(object sender, MouseEventArgs e)
        {
            label1.Text = e.X.ToString();
            label2.Text = e.Y.ToString();

            if(mouse_X != e.X || mouse_Y != e.Y)
            {
                if(choice_figure)
                {
                    choice_figure = false;
                    int border = all_figures.Count() - num_selected_figures;
                    if(choice_figure_id < border)
                    {
                        all_figures.Add(all_figures[choice_figure_id]);
                        all_figures.RemoveAt(choice_figure_id);
                        ++ num_selected_figures;
                        switch_activity();
                    }
                }

                if(mouse_drag)
                    lock(locker)
                    {
                        int axis_X = e.X - mouse_X,
                            axis_Y = e.Y - mouse_Y;
                        Animation animation = new Animation();
                        animation.move_delta_X = axis_X;
                        animation.move_delta_Y = axis_Y;
                        animation.move_priority = 1;

                        Primitive OBJ = new Primitive();
                        int num_figures = all_figures.Count(),
                            border = num_figures - num_selected_figures;
                        for(int F = border, num_polygons, P; F < num_figures; ++ F)
                        {
                            OBJ = all_figures[F];
                            num_polygons = OBJ.multi_figure.Count();
                            for(P = 0; P < num_polygons; ++ P)
                                OBJ.Polygon_Transformation(OBJ.multi_figure[P], animation);

                            OBJ.Polygon_Transformation(OBJ.skelet, animation);

                            if(OBJ.run_animations.Count() > 0)
                            {
                                OBJ.run_animations[0].axis_X_zoom += axis_X;
                                OBJ.run_animations[0].axis_Y_zoom += axis_Y;
                                OBJ.run_animations[0].axis_X_turn += axis_X;
                                OBJ.run_animations[0].axis_Y_turn += axis_Y;
                            }
                        }
                        mouse_X = e.X;
                        mouse_Y = e.Y;
                    }
            }

            if(creating) // Стрелка и равнобедренный треугольник
            {
                ArPoints[1] = new Point(e.X, e.Y);
                all_figures[all_figures.Count() - 1].update_creating(e.X, e.Y);
            }
        }

        // Обработчик отпускания кнопок мыши
        private void pbDrawLine_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                mouse_drag = false;

                if(choice_figure)
                {
                    choice_figure = false;
                    int border = all_figures.Count() - num_selected_figures;
                    if(choice_figure_id < border)
                    {
                        all_figures.Add(all_figures[choice_figure_id]);
                        all_figures.RemoveAt(choice_figure_id);
                        ++ num_selected_figures;
                    }
                    else
                    {
                        all_figures.Insert(border, all_figures[choice_figure_id]);
                        all_figures.RemoveAt(choice_figure_id + 1);
                        -- num_selected_figures;
                    }
                    switch_activity();
                }

                if(creating)
                {
                    ArPoints.Clear();
                    if(!all_figures[all_figures.Count() - 1].end_creating())
                    {
                        all_figures.RemoveAt(all_figures.Count() - 1);
                        -- num_selected_figures;
                        switch_activity();
                    }
                    creating = false;
                }
            }
            if(e.Button == MouseButtons.Right)
            {
                if(!context_menu_block)     contextMenuStrip1.Show(MousePosition);
                else                        context_menu_block = false;
            }
        }

        // Обработчик нажатия кнопок мыши
        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Middle)
                if(wheel_mode)  wheel_mode = false;
                else            wheel_mode = true;

            if(e.Button == MouseButtons.Left)
            {
                int X = e.X;
                int Y = e.Y;

                if(relative_figure_create)
                    switch(type_transformations)
                    {
                        case TURN_30_CLOCK_WISE:
                        case TURN_30_COUNTER_CW:
                        {
                            Crosshair relative_figure = new Crosshair(new PointD(e.X, e.Y));
                            // relative_figure.Push(new PointD(e.X, e.Y));
                            relative_figure_init(relative_figure);
                            return; // <<<
                        }
                        case MIRROR_VERTICAL:
                        {
                            Straight_Vertival relative_figure = new Straight_Vertival();
                            relative_figure.Push(new PointD(e.X, e.Y));
                            relative_figure_init(relative_figure);
                            return; // <<<
                        }
                        case MIRROR_RANDOM:
                        {
                            if(ArPoints.Count() > 0)
                            {
                                Straight_Random relative_figure = new Straight_Random();
                                ArPoints.Add(new Point(e.X, e.Y));
                                relative_figure.Push(ArPoints);
                                ArPoints.Clear();
                                relative_figure_init(relative_figure);
                                return; // <<<
                            }
                            break;
                        }
                    }

                for(int id = all_figures.Count() - 1; id >= 0; -- id)
                    if(all_figures[id].Is_Include(X, Y))
                    {
                        mouse_drag = true;

                        if(System.Windows.Forms.Control.ModifierKeys != Keys.Control)
                        {
                            all_figures.Add(all_figures[id]);
                            all_figures.RemoveAt(id);
                            num_selected_figures = 1;
                            switch_activity();
                        }
                        else
                        {
                            choice_figure = true;
                            choice_figure_id = id;
                        }

                        mouse_X = X;
                        mouse_Y = Y;

                        return; // <<<
                    }

                int last_id = ArPoints.Count() - 1;
                if(last_id >= 0
                && ArPoints[last_id].X == e.X
                && ArPoints[last_id].Y == e.Y)  ArPoints.RemoveAt(last_id);
                else
                {
                    int num = ArPoints.Count();
                    if(num < 100)
                    {
                        bool block = false;
                        for(int p = 0; p < num; ++ p) // Устранение пересечений.
                        {
                            if(ArPoints[p].X == e.X
                            && ArPoints[p].Y == e.Y)
                                block = true;
                        }

                        /* Устранение пересечений.
                        if(num > 1)
                        {
                            double S  = 0.0;

                            double X1 = ArPoints[num - 1].X;
                            double Y1 = ArPoints[num - 1].Y;
                            double X2 = e.X;
                            double Y2 = e.Y;
                            for(int p = 0; p < num - 1; ++ p)
                            {
                                S = 1.0 / 2.0 * Math.Abs((X2 - X1) * (ArPoints[p].Y - Y1) - (ArPoints[p].X - X1) * (Y2 - Y1));
                                if(S < 0.001) // Лежит ли одна из точек и последний отрезок на одной прямой. Тут нужно проверять и отрезок между первой и последней вершиной.
                                {
                                    block = true;
                                    break;
                                }
                            }

                            double x1 = 0.0;
                            double y1 = 0.0;
                            double x2 = 0.0;
                            double y2 = 0.0;
                            double x3 = ArPoints[num - 1].X;
                            double y3 = ArPoints[num - 1].Y;
                            double x4 = e.X;
                            double y4 = e.Y;
                            double k1 = 0.0;
                            double k2 = 0.0;
                            if(!block)
                                for(int p = 1; p < num; ++ p)
                                {
                                    x1 = ArPoints[p - 1].X;
                                    y1 = ArPoints[p - 1].Y;
                                    x2 = ArPoints[p    ].X;
                                    y2 = ArPoints[p    ].Y;

                                    k1 = (y1 - y2) / (x1 - x2);
                                    k2 = (y3 - y4) / (x3 - x4);

                                    if(Math.Abs(k1 - k2) < 0.001) // Параллельные отрезки.
                                    {
                                        block = true;
                                        break;
                                    }

                                    S = 1.0 / 2.0 * Math.Abs((x2 - x1) * (y4 - y1) - (x4 - x1) * (y2 - y1));
                                    if(S < 0.001) // Новая точка лежит на одной прямой с одним из отрезков.
                                    {
                                        block = true;
                                        break;
                                    }
                                }
                        }
                        */

                        if(!block)              ArPoints.Add(new Point(e.X, e.Y));
                    }
                }

                switch(type_primitive)
                {
                    case LINE:                  // Отрезок
                    {
                        if(ArPoints.Count() > 1) // Конец ввода
                        {
                            Line_Segment OBJ = new Line_Segment();
                            OBJ.Push(ArPoints);
                            primitive_init(OBJ, true);
                        }
                        break;
                    }
                    case TRIANGLE:              // Равнобедренный треугольник
                    {
                        if(!creating)
                        {
                            creating = true;
                            Triangle_Isosceles OBJ = new Triangle_Isosceles();
                            OBJ.start_creating(e.X, e.Y);
                            primitive_init(OBJ, false);
                        }
                        break;
                    }
                    case ARROW:                 // Стрелка
                    {
                        creating = true;
                        Arrow OBJ = new Arrow();
                        OBJ.start_creating(e.X, e.Y);
                        primitive_init(OBJ, false);
                        break;
                    }
                    case CROSSHAIR:             // Перекрестие
                    {
                        creating = true;
                        Crosshair OBJ = new Crosshair();
                        OBJ.start_creating(e.X, e.Y);
                        primitive_init(OBJ, false);
                        break;
                    }
                    case VERTIVAL_STRAIGHT:     // Вертикальная прямая
                    {
                        Straight_Vertival OBJ = new Straight_Vertival();
                        OBJ.Push(ArPoints);
                        primitive_init(OBJ, true);
                        break;
                    }
                    case RANDOM_STRAIGHT:       // Произвольная прямая
                    {
                        if(ArPoints.Count() > 1) // Конец ввода
                        {
                            Straight_Random OBJ = new Straight_Random();
                            OBJ.Push(ArPoints);
                            primitive_init(OBJ, true);
                        }
                        break;
                    }
                    case CIRCLE:                // Круг
                    {
                        creating = true;
                        Circle OBJ = new Circle();
                        OBJ.start_creating(e.X, e.Y);
                        primitive_init(OBJ, false);
                        break;
                    }
                }
            }

            if(e.Button == MouseButtons.Right)
            {
                if(ArPoints.Count() > 2) // Конец ввода
                    switch(type_primitive)
                    {
                        case CURVE:     // Кривая Безье
                        {
                            Bezier_Сurve OBJ = new Bezier_Сurve();
                            OBJ.Push(ArPoints);
                            primitive_init(OBJ, true);
                            context_menu_block = true;
                            return; // <<<
                        }
                        case POLYGON:   // Многоугольник
                        {
                            Multi_Figure OBJ = new Multi_Figure();

                            try
                            {
                                OBJ.Push_Check(ArPoints);
                            }
                            catch(Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            primitive_init(OBJ, true);
                            context_menu_block = true;
                            return; // <<<
                        }
                    }

                switch(type_transformations)
                {
                    case TURN_30_CLOCK_WISE:    { turn_30(true);        break; }
                    case TURN_30_COUNTER_CW:    { turn_30(false);       break; }
                    case MIRROR_VERTICAL:       { mirror_vertical();    break; }
                    case MIRROR_RANDOM:         { mirror_random();      break; }
                }
            }
        }

        private void primitive_init(Primitive OBJ, bool end)
        {
            OBJ.pen.Color = draw_pen.Color;
            OBJ.pen.Width = draw_pen.Width;

            all_figures.Add(OBJ);

            if(System.Windows.Forms.Control.ModifierKeys != Keys.Control)
                    num_selected_figures = 1;
            else    num_selected_figures ++;
            switch_activity();

            if(end)     ArPoints.Clear();
            else        ArPoints.Add(ArPoints[0]);
        }

// Выбор типа примитива ---------------------------------------------------------------

        private void radioButton1_CheckedChanged(object sender, EventArgs e) {
            type_primitive = LINE;                  ArPoints.Clear();       // Отрезок
            line_segment_ToolStripMenuItem.         Checked = true; }
        private void radioButton2_CheckedChanged(object sender, EventArgs e) {
            type_primitive = CURVE;                 ArPoints.Clear();       // Кривая Безье
            bezier_curve_ToolStripMenuItem.         Checked = true; }
        private void radioButton3_CheckedChanged(object sender, EventArgs e) {
            type_primitive = POLYGON;               ArPoints.Clear();       // Многоугольник
            polygon_ToolStripMenuItem.              Checked = true; }
        private void radioButton4_CheckedChanged(object sender, EventArgs e) {
            type_primitive = TRIANGLE;              ArPoints.Clear();       // Равнобедреннный треугольник
            triangle_isosceles_ToolStripMenuItem.   Checked = true; }
        private void radioButton5_CheckedChanged(object sender, EventArgs e) {
            type_primitive = ARROW;                 ArPoints.Clear();       // Стрелка
            arrow_ToolStripMenuItem.                Checked = true; }

        private void radioButton6_CheckedChanged(object sender, EventArgs e) {
            type_primitive = CROSSHAIR;             ArPoints.Clear(); }   // Перекрестие
        private void radioButton7_CheckedChanged(object sender, EventArgs e) {
            type_primitive = VERTIVAL_STRAIGHT;     ArPoints.Clear(); } // Вертикальная прямая
        private void radioButton8_CheckedChanged(object sender, EventArgs e) {
            type_primitive = RANDOM_STRAIGHT;       ArPoints.Clear(); } // Произвольная прямая
        private void radioButton9_CheckedChanged(object sender, EventArgs e) {
            type_primitive = CIRCLE;                ArPoints.Clear(); } // Круг

// Общие элементы формы ---------------------------------------------------------------------------

        // Вкл/Выкл кнопки ТМО и пункты ТМО в контекстном меню
        private void switch_activity()
        {
            bool enabled = false;

            if(num_selected_figures == 2)
            {
                int num_figures = all_figures.Count();
                if(all_figures[num_figures - 1].type == 0
                || all_figures[num_figures - 2].type == 0)
                    enabled = true;
            }

            button3.Enabled = enabled;
            button4.Enabled = enabled;
            button5.Enabled = enabled;
            button6.Enabled = enabled;
            button7.Enabled = enabled;

            STO_ToolStripMenuItem.Enabled = enabled;
        }

        // Обработчик события выбора цвета в элементе colorDialog1
        private void button10_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog() == DialogResult.Cancel)    return;
            draw_pen.Color = colorDialog1.Color;
        }

        // Тест - Линии между точками при создании примитива
        private void checkBox1_Click(object sender, EventArgs e)
        {
            if(checkBox1.Checked)   g_test_paint = true;
            else                    g_test_paint = false;
        }

        // Очистка окна
        private void button1_Click(object sender, EventArgs e)
        {
            screen_width  = this.Width  - diff_form_screen_X;
            screen_height = this.Height - diff_form_screen_Y;
            this.pbDrawLine.Width  = screen_width;
            this.pbDrawLine.Height = screen_height;
            bmp = new Bitmap        (screen_width,
                                     screen_height);
            g = Graphics.FromImage(bmp);
            pbDrawLine.Image = bmp;

            ArPoints.Clear();

            all_figures.Clear();
            num_selected_figures = 0;
            switch_activity();

            type_transformations = -1;
            relative_figure_create = false;
        }

        // Удаление выбранных фигур
        private void button9_Click(object sender, EventArgs e)
        {
            int num_figures = all_figures.Count();
            int border = num_figures - num_selected_figures;
            for(int F = num_figures - 1; F >= border; -- F)
            {
                if(all_figures[F].type == 1)
                    type_transformations = -1;
                all_figures.RemoveAt(F);
            }
            num_selected_figures = 0;
            switch_activity();
        }

// Проверка ввода. --------------------------------------------------------------------------------

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void textBox1_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox2_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox4_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox5_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox6_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox7_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox9_TextChanged (object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox11_TextChanged(object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox12_TextChanged(object sender, EventArgs e) { filter_double_input(sender, e); }
        private void textBox15_TextChanged(object sender, EventArgs e) { filter_uint_input  (sender, e); }
        private void textBox14_TextChanged(object sender, EventArgs e) { filter_uint_input  (sender, e); }

        private void filter_double_input(object sender, EventArgs e) {
            string txt = ((TextBox)sender).Text;
            if(txt == "")   errorProvider1.Clear();
            else
                try {
                    double.Parse(txt);
                    errorProvider1.Clear(); }
                catch(FormatException exc)  {
                    errorProvider1.SetError((TextBox)sender, exc.Message); }}

        private void filter_uint_input(object sender, EventArgs e) {
            string txt = ((TextBox)sender).Text;
            if(txt == "")   errorProvider1.Clear();
            else
                try {
                    uint.Parse(txt);
                    errorProvider1.Clear(); }
                catch(FormatException exc)  {
                    errorProvider1.SetError((TextBox)sender, exc.Message); }}

        private void textBox1_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox2_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox4_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox5_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox6_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox7_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox9_Validating (object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox11_Validating(object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox12_Validating(object sender, CancelEventArgs e) { filter_double_end(sender, e); }
        private void textBox15_Validating(object sender, CancelEventArgs e) { filter_uint_end  (sender, e); }
        private void textBox14_Validating(object sender, CancelEventArgs e) { filter_uint_end  (sender, e); }

        private void filter_double_end(object sender, CancelEventArgs e) {
            string txt = ((TextBox)sender).Text;
            if(txt == "")   errorProvider1.Clear();
            else
                try {
                    double.Parse(txt);
                    errorProvider1.Clear(); }
                catch(FormatException exc)  {
                    errorProvider1.SetError((TextBox)sender, exc.Message);
                    e.Cancel = true; }}

        private void filter_uint_end(object sender, CancelEventArgs e) {
            string txt = ((TextBox)sender).Text;
            if(txt == "")   errorProvider1.Clear();
            else
                try {
                    uint.Parse(txt);
                    errorProvider1.Clear(); }
                catch(FormatException exc)  {
                    errorProvider1.SetError((TextBox)sender, exc.Message);
                    e.Cancel = true; }}

// Автоматическое увеличение области изображения. -------------------------------------------------

        // Обработчик изменения размера формы.
        private void Form1_ClientSizeChanged(object sender, EventArgs e)
        {
            int form_width  = this.Width,
                form_height = this.Height;

            this.panel1.Size = new System.Drawing.Size(form_width  - diff_form_screen_X,
                                                       form_height - diff_form_screen_Y);

            if(screen_width  + diff_form_screen_X < form_width
            || screen_height + diff_form_screen_Y < form_height)
            {
                screen_width  = form_width  - diff_form_screen_X;
                screen_height = form_height - diff_form_screen_Y;
                this.pbDrawLine.Width  = screen_width;
                this.pbDrawLine.Height = screen_height;
                bmp = new Bitmap        (screen_width,
                                         screen_height);
                g = Graphics.FromImage(bmp);
                pbDrawLine.Image = bmp; // Фикс для VS 2015
                g.DrawImage(pbDrawLine.Image, 0, 0);
            }
        }

// Геометрические преобразования. -----------------------------------------------------------------

        // В очередь
        private void button2_Click(object sender, EventArgs e)
        {
            int num_figures = all_figures.Count();
            int border = num_figures - num_selected_figures;
            for(int F = border; F < num_figures; ++ F)
                Set_Animation(all_figures[F]);
        }

        // Запуск
        private void button8_Click(object sender, EventArgs e)
        {
            lock(locker)
            {
                int num_figures = all_figures.Count(),
                    border = num_figures - num_selected_figures,
                    num_animations = 0,
                    F = 0,
                    A = 0;

                for(F = border; F < num_figures; ++ F)
                {
                    if(all_figures[F].queue_animations.Count() == 0)
                        Set_Animation(all_figures[F]);

                    all_figures[F].run_animations.Clear();
                    num_animations = all_figures[F].queue_animations.Count();
                    for(A = 0; A < num_animations; ++ A)
                    {
                        all_figures[F].run_animations.Add(new Animation());
                        all_figures[F].run_animations[A].Copy(all_figures[F].queue_animations[A]);
                    }
                    all_figures[F].queue_animations.Clear();

                    all_figures[F].start_animation();
                }
            }
        }

        // Стоп
        private void button11_Click(object sender, EventArgs e)
        {
            lock(locker)
            {
                int num_figures = all_figures.Count(),
                    border = num_figures - num_selected_figures;

                for(int F = border; F < num_figures; ++ F)
                    all_figures[F].run_animations.Clear();
            }
        }

        private void Set_Animation(Primitive primitive)
        {
            Animation animation = new Animation();

            primitive.Center = primitive.Get_Center();

            if(textBox1.Text != "")     animation.move_delta_X = double.Parse(textBox1.Text);
            if(textBox2.Text != "")     animation.move_delta_Y = double.Parse(textBox2.Text);
            if(textBox4.Text != "")     animation.zoom_delta_X = double.Parse(textBox4.Text);
            if(textBox5.Text != "")     animation.zoom_delta_Y = double.Parse(textBox5.Text);
            if(textBox9.Text != "")     animation.turn_delta   = double.Parse(textBox9.Text);

            if(animation.move_delta_X != 0
            || animation.move_delta_Y != 0)     animation.move_priority = comboBox1.SelectedIndex + 1;
            if(animation.zoom_delta_X != 1
            || animation.zoom_delta_Y != 1)     animation.zoom_priority = comboBox2.SelectedIndex + 1;
            if(animation.turn_delta   != 0)     animation.turn_priority = comboBox3.SelectedIndex + 1;

            if(animation.move_priority != -1
            || animation.zoom_priority != -1
            || animation.turn_priority != -1)
            {
                string axis_text;
                if((axis_text = textBox6. Text) == "")      animation.axis_X_zoom = primitive.Center.X;
                else                                        animation.axis_X_zoom = double.Parse(axis_text);
                if((axis_text = textBox7. Text) == "")      animation.axis_Y_zoom = primitive.Center.Y;
                else                                        animation.axis_Y_zoom = double.Parse(axis_text);
                if((axis_text = textBox11.Text) == "")      animation.axis_X_turn = primitive.Center.X;
                else                                        animation.axis_X_turn = double.Parse(axis_text);
                if((axis_text = textBox12.Text) == "")      animation.axis_Y_turn = primitive.Center.Y;
                else                                        animation.axis_Y_turn = double.Parse(axis_text);

                string text;
                if((text = textBox15.Text) != "")  animation.iterations = int.Parse(text);
                if((text = textBox14.Text) != "")  animation.time_out   = int.Parse(text);

                if(animation.iterations <= 0)   animation.iterations = 1;
                if(animation.time_out   <= 0)   animation.time_out   = 1;

                primitive.add_animation(animation);
            }
        }

// Теоретико-множественные операции ---------------------------------------------------------------

        private void Check_Figures(int FC, int FS)
        {
            // all_figures[FC].multi_figure.Insert(0, all_figures[FC].skelet);
            // all_figures[FS].multi_figure.Insert(0, all_figures[FS].skelet);
            // all_figures[FC].multi_figure = all_figures[FC].template;
            // all_figures[FS].multi_figure = all_figures[FS].template;
            if(all_figures[FC].GetType() == typeof(Triangle_Isosceles))
            {
                double HALF_WIDTH_TEMPLATE  = Math.Abs(all_figures[FC].skelet[1].X - all_figures[FC].skelet[0].X);
                double HALF_HEIGHT_TEMPLATE = Math.Abs(all_figures[FC].skelet[2].Y - all_figures[FC].skelet[0].Y);

                all_figures[FC].multi_figure.Clear();
                all_figures[FC].multi_figure.Add(new List<PointD>());

                all_figures[FC].multi_figure[0].Add(new PointD(all_figures[FC].skelet[0].X - HALF_WIDTH_TEMPLATE, all_figures[FC].skelet[2].Y));
                all_figures[FC].multi_figure[0].Add(new PointD(all_figures[FC].skelet[0].X, all_figures[FC].skelet[0].Y - HALF_HEIGHT_TEMPLATE));
                all_figures[FC].multi_figure[0].Add(new PointD(all_figures[FC].skelet[1].X, all_figures[FC].skelet[2].Y));
            }
            if(all_figures[FS].GetType() == typeof(Triangle_Isosceles))
            {
                double HALF_WIDTH_TEMPLATE  = Math.Abs(all_figures[FS].skelet[1].X - all_figures[FS].skelet[0].X);
                double HALF_HEIGHT_TEMPLATE = Math.Abs(all_figures[FS].skelet[2].Y - all_figures[FS].skelet[0].Y);

                all_figures[FS].multi_figure.Clear();
                all_figures[FS].multi_figure.Add(new List<PointD>());

                all_figures[FS].multi_figure[0].Add(new PointD(all_figures[FS].skelet[0].X - HALF_WIDTH_TEMPLATE, all_figures[FS].skelet[2].Y));
                all_figures[FS].multi_figure[0].Add(new PointD(all_figures[FS].skelet[0].X, all_figures[FS].skelet[0].Y - HALF_HEIGHT_TEMPLATE));
                all_figures[FS].multi_figure[0].Add(new PointD(all_figures[FS].skelet[1].X, all_figures[FS].skelet[2].Y));
            }

            if(all_figures[FC].GetType() == typeof(Line_Segment))   all_figures[FC].Convert();
            if(all_figures[FS].GetType() == typeof(Line_Segment))   all_figures[FS].Convert();
        }

        private void button3_Click(object sender, EventArgs e) { // Объединение
            try
            {
                int FC = all_figures.Count() - 1,
                    FS = FC - 1;
                Check_Figures(FC, FS);
                List<Broken_Line> broken_lines = new List<Broken_Line>();
                broken_lines.AddRange(STO.Get_Borders(all_figures, FC, FS, true));
                broken_lines.AddRange(STO.Get_Borders(all_figures, FS, FC, true));
                STO_finish(broken_lines, FC, FS);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e) { // Разность A / B
            try
            {
                int FC = all_figures.Count() - 1,
                    FS = FC - 1;
                Check_Figures(FC, FS);
                all_figures[FS].Reverse();
                List<Broken_Line> broken_lines = new List<Broken_Line>();
                broken_lines.AddRange(STO.Get_Borders(all_figures, FC, FS, true));
                broken_lines.AddRange(STO.Get_Borders(all_figures, FS, FC, false));
                STO_finish(broken_lines, FC, FS);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e) { // Разность B / A
            try
            {
                int FC = all_figures.Count() - 2,
                    FS = FC + 1;
                Check_Figures(FC, FS);
                all_figures[FS].Reverse();
                List<Broken_Line> broken_lines = new List<Broken_Line>();
                broken_lines.AddRange(STO.Get_Borders(all_figures, FC, FS, true));
                broken_lines.AddRange(STO.Get_Borders(all_figures, FS, FC, false));
                STO_finish(broken_lines, FC, FS);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button6_Click(object sender, EventArgs e) { // Симметрическая разность
            try
            {
                int FC = all_figures.Count() - 1,
                    FS = FC - 1;
                Check_Figures(FC, FS);
                all_figures[FS].Reverse();
                List<Broken_Line> broken_lines = new List<Broken_Line>();
                broken_lines.AddRange(STO.Get_Borders(all_figures, FC, FS, true));
                broken_lines.AddRange(STO.Get_Borders(all_figures, FS, FC, false));
                all_figures[FC].Reverse();
                all_figures[FS].Reverse();
                broken_lines.AddRange(STO.Get_Borders(all_figures, FC, FS, false));
                broken_lines.AddRange(STO.Get_Borders(all_figures, FS, FC, true));
                STO_finish(broken_lines, FC, FS);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e) { // Пересечение
            try
            {
                int FC = all_figures.Count() - 1,
                    FS = FC - 1;
                Check_Figures(FC, FS);
                List<Broken_Line> broken_lines = new List<Broken_Line>();
                broken_lines.AddRange(STO.Get_Borders(all_figures, FC, FS, false));
                broken_lines.AddRange(STO.Get_Borders(all_figures, FS, FC, false));
                STO_finish(broken_lines, FC, FS);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void STO_finish(List<Broken_Line> broken_lines, int FC, int FS)
        {
            if(FS > FC)
            {
                all_figures.RemoveAt(FS);
                all_figures.RemoveAt(FC);
            }
            else
            {
                all_figures.RemoveAt(FC);
                all_figures.RemoveAt(FS);
            }
            Multi_Figure temp = new Multi_Figure();
            temp.multi_figure.AddRange(STO.collector(broken_lines));
            all_figures.Add(temp);
            all_figures[all_figures.Count() - 1].pen.Color = draw_pen.Color;
            num_selected_figures = 1;
            switch_activity();
        }

// Главное меню --------------------------------------------------------------------------------

        // Файл - Открыть
        private void open_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = null;
                if((fs = (FileStream)openFileDialog1.OpenFile()) != null)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    try
                    {
                        button1_Click(sender, e); // Очистить окно

                        num_selected_figures = (int)formatter.Deserialize(fs);
                        switch_activity();
                        all_figures = (List<Primitive>)formatter.Deserialize(fs);

                        ArPoints.Clear();
                        type_transformations = -1;
                        relative_figure_create = false;
                        int relative_figure_id = find_relative();
                        if(relative_figure_id != -1)
                            all_figures.RemoveAt(relative_figure_id);

                        turn_30_clock_wise_ToolStripMenuItem.Checked = false;
                        turn_30_counter_cw_ToolStripMenuItem.Checked = false;
                        mirror_vertical_ToolStripMenuItem.Checked    = false;
                        mirror_random_ToolStripMenuItem.Checked      = false;
                        checkBox2.Checked = false;
                        checkBox3.Checked = false;
                        checkBox4.Checked = false;
                        checkBox5.Checked = false;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    fs.Close();
                }
            }
        }

        // Файл Сохранить как...
        private void save_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if(saveFileDialog1.FilterIndex == 1)
                {
                    FileStream fs = null;
                    if((fs = (FileStream)saveFileDialog1.OpenFile()) != null)
                    {
                        int num_figures = all_figures.Count();
                        for(int F = 0; F < num_figures; ++ F)
                        {
                            all_figures[F].color = all_figures[F].pen.Color;
                            all_figures[F].width = all_figures[F].pen.Width;
                        }

                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(fs, num_selected_figures);
                        formatter.Serialize(fs, all_figures);
                        fs.Close();
                    }
                }
                else
                {
                    Bitmap bmp = (Bitmap)pbDrawLine.Image;
                    String path = saveFileDialog1.FileName;
                    bmp.Save(path, System.Drawing.Imaging.ImageFormat.Bmp);
                }
                saveFileDialog1.FileName = "Безымянный";
            }
        }

        // О программе...
        private void about_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 about = new Form2();
            about.ShowDialog();
        }

// Обработка клавиши Enter CheckBox-ми ------------------------------------------------------------

        private void checkBox1_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                if(checkBox1.Checked)   checkBox1.Checked = false;
                else                    checkBox1.Checked = true;
                checkBox1_Click(sender, e); }}

        private void checkBox2_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                if(checkBox2.Checked)   checkBox2.Checked = false;
                else                    checkBox2.Checked = true;
                checkBox2_Click(sender, e); }}

        private void checkBox3_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                if(checkBox3.Checked)   checkBox3.Checked = false;
                else                    checkBox3.Checked = true;
                checkBox3_Click(sender, e); }}

        private void checkBox4_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                if(checkBox4.Checked)   checkBox4.Checked = false;
                else                    checkBox4.Checked = true;
                checkBox4_Click(sender, e); }}

        private void checkBox5_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                if(checkBox5.Checked)   checkBox5.Checked = false;
                else                    checkBox5.Checked = true;
                checkBox5_Click(sender, e); }}

// Контекстное меню -------------------------------------------------------------------------------

        private void union_ToolStripMenuItem_Click          (object sender, EventArgs e) {
            button3_Click(sender, e); }                     // ТМО объединение
        private void diff12_ToolStripMenuItem_Click         (object sender, EventArgs e) {
            button3_Click(sender, e); }                     // ТМО разность 1 - 2
        private void diff21_ToolStripMenuItem_Click         (object sender, EventArgs e) {
            button3_Click(sender, e); }                     // ТМО разность 2 - 1
        private void diff_symm_ToolStripMenuItem_Click      (object sender, EventArgs e) {
            button3_Click(sender, e); }                     // ТМО симметрическая разность
        private void intersection_ToolStripMenuItem_Click   (object sender, EventArgs e) {
            button3_Click(sender, e); }                     // ТМО пересечение

        private void line_segment_ToolStripMenuItem_Click       (object sender, EventArgs e) {
            radioButton1.Checked = true; }                      // Отрезок
        private void bezier_curve_ToolStripMenuItem_Click       (object sender, EventArgs e) {
            radioButton2.Checked = true; }                      // Кривая Безье
        private void polygon_ToolStripMenuItem_Click            (object sender, EventArgs e) {
            radioButton3.Checked = true; }                      // Многоугольник
        private void triangle_isosceles_ToolStripMenuItem_Click (object sender, EventArgs e) {
            radioButton4.Checked = true; }                      // Равнобедренный треугольник
        private void arrow_ToolStripMenuItem_Click              (object sender, EventArgs e) {
            radioButton5.Checked = true; }                      // Стрелка

        // Поворот на 30 градусов по часовой стрелке
        private void turn_30_clock_wise_ToolStripMenuItem_Click(object sender, EventArgs e) {
            if(turn_30_clock_wise_ToolStripMenuItem.Checked == true) {
                    checkBox2.Checked = true; }
            else    checkBox2.Checked = false;
            checkBox2_Click(sender, e);
        }
        // Поворот на 30 градусов против часовой стрелки
        private void turn_30_counter_cw_ToolStripMenuItem_Click(object sender, EventArgs e) {
            if(turn_30_counter_cw_ToolStripMenuItem.Checked == true) {
                    checkBox3.Checked = true; }
            else    checkBox3.Checked = false;
            checkBox3_Click(sender, e);
        }
        // Отражение от вертикальной прямой
        private void mirror_vertical_ToolStripMenuItem_Click(object sender, EventArgs e) {
            if(mirror_vertical_ToolStripMenuItem.Checked == true) {
                    checkBox4.Checked = true; }
            else    checkBox4.Checked = false;
            checkBox4_Click(sender, e);
        }
        // Отражение от произвольной прямой
        private void mirror_random_ToolStripMenuItem_Click(object sender, EventArgs e) {
            if(mirror_random_ToolStripMenuItem.Checked == true) {
                    checkBox5.Checked = true; }
            else    checkBox5.Checked = false;
            checkBox5_Click(sender, e);
        }

        // Анимация - В очередь
        private void in_queue_ToolStripMenuItem_Click(object sender, EventArgs e) {
            button2_Click(sender, e); }
        // Анимация - Запуск
        private void start_ToolStripMenuItem_Click(object sender, EventArgs e) {
            button8_Click(sender, e); }
        // Анимация - Стоп
        private void stop_ToolStripMenuItem_Click(object sender, EventArgs e) {
            button11_Click(sender, e); }

        // Очистить окно
        private void clear_window_ToolStripMenuItem_Click(object sender, EventArgs e) {
            button1_Click(sender, e); }
        // Отмена
        private void cancel_ToolStripMenuItem_Click(object sender, EventArgs e) { }

// Шаблоны преобразований -------------------------------------------------------------------------

        // Поворот на 30 градусов по часовой стрелке
        private void checkBox2_Click(object sender, EventArgs e) {
            if(checkBox2.Checked) {
                 type_transformations = TURN_30_CLOCK_WISE;
                 preparation    (turn_30_clock_wise_ToolStripMenuItem); }
            else cancel_relative(turn_30_clock_wise_ToolStripMenuItem);
        }
        // Поворот на 30 градусов против часовой стрелки
        private void checkBox3_Click(object sender, EventArgs e) {
            if(checkBox3.Checked) {
                 type_transformations = TURN_30_COUNTER_CW;
                 preparation    (turn_30_counter_cw_ToolStripMenuItem); }
            else cancel_relative(turn_30_counter_cw_ToolStripMenuItem);
        }
        // Отражение от вертикальной прямой
        private void checkBox4_Click(object sender, EventArgs e) {
            if(checkBox4.Checked) {
                 type_transformations = MIRROR_VERTICAL;
                 preparation    (mirror_vertical_ToolStripMenuItem); }
            else cancel_relative(mirror_vertical_ToolStripMenuItem);
        }
        // Отражение от произвольной прямой
        private void checkBox5_Click(object sender, EventArgs e) {
            if(checkBox5.Checked) {
                 type_transformations = MIRROR_RANDOM;
                 preparation    (mirror_random_ToolStripMenuItem); }
            else cancel_relative(mirror_random_ToolStripMenuItem);
        }

        private void preparation(object item_menu)
        {
            foreach(ToolStripMenuItem item in transformation_templates_ToolStripMenuItem.DropDownItems)
                item.Checked = false;
            ((ToolStripMenuItem)item_menu).Checked = true;
            ((ToolStripMenuItem)item_menu).CheckState = CheckState.Indeterminate;;

            ArPoints.Clear();

            if(TURN_30_CLOCK_WISE != type_transformations)  checkBox2.Checked = false;
            if(TURN_30_COUNTER_CW != type_transformations)  checkBox3.Checked = false;
            if(MIRROR_VERTICAL    != type_transformations)  checkBox4.Checked = false;
            if(MIRROR_RANDOM      != type_transformations)  checkBox5.Checked = false;

            relative_figure_create = true;
            delete_relative();
        }

        private void cancel_relative(object item_menu)
        {
            ((ToolStripMenuItem)item_menu).Checked = false;

            type_transformations = -1;
            if(relative_figure_create)
            {
                ArPoints.Clear();
                relative_figure_create = false;
            }
            else    delete_relative();
        }

        private void delete_relative()
        {
            int relative_figure_id = find_relative();
            if(relative_figure_id != -1)
            {
                int border = all_figures.Count() - num_selected_figures;
                if(relative_figure_id >= border)
                {
                    -- num_selected_figures;
                    switch_activity();
                }
                all_figures.RemoveAt(relative_figure_id);
            }
        }

        private int find_relative()
        {
            int num_figures = all_figures.Count();
            for(int F = 0; F < num_figures; ++ F)
                if(all_figures[F].type == 1)    return F;
            return -1;
        }

        private void relative_figure_init(Primitive OBJ)
        {
            OBJ.type = 1;
            relative_figure_create = false;
            OBJ.pen.Color = Color.Chocolate;
            int border = all_figures.Count() - num_selected_figures;
            all_figures.Insert(border, OBJ);
        }

        private void operation_end(int relative_figure_id, int border)
        {
            context_menu_block = true;

            foreach(ToolStripMenuItem item in transformation_templates_ToolStripMenuItem.DropDownItems)
                    item.Checked = false;
            switch(type_transformations)
            {
                case TURN_30_CLOCK_WISE:    { checkBox2.Checked = false; break; }
                case TURN_30_COUNTER_CW:    { checkBox3.Checked = false; break; }
                case MIRROR_VERTICAL:       { checkBox4.Checked = false; break; }
                case MIRROR_RANDOM:         { checkBox5.Checked = false; break; }
            }
            type_transformations = -1;

            if(relative_figure_id >= border)
            {
                -- num_selected_figures;
                switch_activity();
            }
            all_figures.RemoveAt(relative_figure_id);
        }

        private void turn_30(bool clock_wise)
        {
            if(!relative_figure_create)
                lock(locker)
                {
                    int relative_figure_id = find_relative();

                    Animation animation = new Animation();
                    PointD relative_center = all_figures[relative_figure_id].Get_Center(),
                           figure_center_before = new PointD(),
                           figure_center_after  = new PointD();

                    animation.turn_priority = 1;
                    animation.turn_delta = 30; // Угол поворота в градусах

                    if(!clock_wise)     animation.turn_delta *= -1;

                    Primitive OBJ = new Primitive();
                    int num_figures  = all_figures.Count(),
                        border = num_figures - num_selected_figures;
                    for(int F = border, num_polygons, P; F < num_figures; ++ F)
                    {
                        animation.axis_X_turn = relative_center.X;
                        animation.axis_Y_turn = relative_center.Y;

                        OBJ = all_figures[F];
                        figure_center_before = OBJ.Get_Center();

                        num_polygons = OBJ.multi_figure.Count();
                        for(P = 0; P < num_polygons; ++ P)
                            OBJ.Polygon_Transformation(OBJ.multi_figure[P], animation);

                        OBJ.Polygon_Transformation(OBJ.skelet, animation);

                        if(OBJ.run_animations.Count() > 0)
                        {
                            figure_center_after = OBJ.Get_Center();

                            OBJ.run_animations[0].axis_X_zoom += figure_center_after.X - figure_center_before.X;
                            OBJ.run_animations[0].axis_Y_zoom += figure_center_after.Y - figure_center_before.Y;
                            OBJ.run_animations[0].axis_X_turn += figure_center_after.X - figure_center_before.X;
                            OBJ.run_animations[0].axis_Y_turn += figure_center_after.Y - figure_center_before.Y;
                        }
                    }

                    operation_end(relative_figure_id, border);
                }
        }

        private void mirror_vertical()
        {
            if(!relative_figure_create)
                lock(locker)
                {
                    int relative_figure_id = find_relative();

                    Animation animation = new Animation();
                    int border = 0,
                        num_figures    = 0,
                        num_polygons   = 0,
                        num_animations = 0,
                        F = 0,
                        P = 0,
                        A = 0;
                    PointD center = new PointD();
                    double move_delta_Y = 0.0;

                    animation.zoom_priority = 1;
                    animation.zoom_delta_X =  1.0;
                    animation.zoom_delta_Y = -1.0;

                    animation.move_priority = 2;

                    Primitive OBJ = new Primitive();
                    num_figures  = all_figures.Count();
                    border = num_figures - num_selected_figures;
                    for(F = border; F < num_figures; ++ F)
                    {
                        OBJ = all_figures[F];
                        center = OBJ.Get_Center();

                        move_delta_Y = (all_figures[relative_figure_id].skelet[0].Y - center.Y) * 2;
                        animation.move_delta_Y = move_delta_Y;

                        animation.axis_X_zoom = center.X;
                        animation.axis_Y_zoom = center.Y;

                        num_polygons = OBJ.multi_figure.Count();
                        for(P = 0; P < num_polygons; ++ P)
                            OBJ.Polygon_Transformation(OBJ.multi_figure[P], animation);

                        OBJ.Polygon_Transformation(OBJ.skelet, animation);

                        if(OBJ.run_animations.Count() > 0)
                        {
                            OBJ.run_animations[0].axis_Y_zoom += move_delta_Y;
                            OBJ.run_animations[0].axis_Y_turn += move_delta_Y;

                            num_animations = OBJ.run_animations.Count();
                            for(A = 0; A < num_animations; ++ A)
                                OBJ.run_animations[A].turn_delta *= -1;
                        }
                    }

                    operation_end(relative_figure_id, border);
                }
        }

        private void mirror_random()
        {
            if(!relative_figure_create)
                lock(locker)
                {
                    int relative_figure_id = find_relative();
                    Primitive relative_figure = all_figures[relative_figure_id];

                    Animation animation = new Animation();
                    int border = 0,
                        num_figures    = 0,
                        num_polygons   = 0,
                        num_animations = 0,
                        F = 0,
                        P = 0,
                        A = 0;
                    PointD center = new PointD();
                    double axis_X = 0.0,
                           axis_Y = 0.0,
                           dX = relative_figure.skelet[1].X - relative_figure.skelet[0].X,
                           dY = relative_figure.skelet[1].Y - relative_figure.skelet[0].Y,
                           hypotenuse_straight = Math.Sqrt(dX * dX + dY * dY),
                           hypotenuse_offset = 0.0;

                    animation.zoom_priority = 1;
                    animation.zoom_delta_X = -1.0;
                    animation.zoom_delta_Y =  1.0;

                    animation.turn_priority = 2;
                    animation.turn_delta = -1 * 2 * Math.Atan2(dX, dY) / Math.PI * 180; // Угол поворота в градусах

                    animation.move_priority = 3;

                    Primitive OBJ = new Primitive();
                    num_figures  = all_figures.Count();
                    border = num_figures - num_selected_figures;
                    for(F = border; F < num_figures; ++ F)
                    {
                        OBJ = all_figures[F];
                        center = OBJ.Get_Center();

                        hypotenuse_offset = 2 * (dX * (center.Y - relative_figure.skelet[0].Y) - dY * (center.X - relative_figure.skelet[0].X)) / Math.Sqrt(dX * dX + dY * dY);

                        axis_X =      hypotenuse_offset / hypotenuse_straight * dY;
                        axis_Y = -1 * hypotenuse_offset / hypotenuse_straight * dX;

                        animation.move_delta_X = axis_X;
                        animation.move_delta_Y = axis_Y;

                        animation.axis_X_turn = animation.axis_X_zoom = center.X;
                        animation.axis_Y_turn = animation.axis_Y_zoom = center.Y;

                        num_polygons = OBJ.multi_figure.Count();
                        for(P = 0; P < num_polygons; ++ P)
                            OBJ.Polygon_Transformation(OBJ.multi_figure[P], animation);

                        OBJ.Polygon_Transformation(OBJ.skelet, animation);

                        if(OBJ.run_animations.Count() > 0)
                        {
                            OBJ.run_animations[0].axis_X_zoom += axis_X;
                            OBJ.run_animations[0].axis_Y_zoom += axis_Y;
                            OBJ.run_animations[0].axis_X_turn += axis_X;
                            OBJ.run_animations[0].axis_Y_turn += axis_Y;

                            num_animations = OBJ.run_animations.Count();
                            for(A = 0; A < num_animations; ++ A)
                                OBJ.run_animations[A].turn_delta *= -1;
                        }
                    }

                    operation_end(relative_figure_id, border);
                }
        }

        // Толщина линии
        private void comboBox4_SelectionChangeCommitted(object sender, EventArgs e) {
            draw_pen.Width = Convert.ToSingle(comboBox4.Text);
            pbDrawLine.Focus();
        }

        // Блокирется переключение фокуса на ComboBox-ы
        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e) {
            pbDrawLine.Focus(); }
        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e) {
            pbDrawLine.Focus(); }
        private void comboBox3_SelectionChangeCommitted(object sender, EventArgs e) {
            pbDrawLine.Focus(); }

        private void button12_Click(object sender, EventArgs e)
        {
            List<List<PointD>> figure1 = new List<List<PointD>>();
            List<PointD> list1 = new List<PointD>{new PointD(200, 200), new PointD(500, 200), new PointD(500, 300), new PointD(200, 300)};
            Primitive.Push_Polygon(ref figure1, list1);

            Multi_Figure temp1 = new Multi_Figure();
            temp1.multi_figure.AddRange(figure1);

            all_figures.Add(temp1);

            List<List<PointD>> figure2 = new List<List<PointD>>();
            List<PointD> list2 = new List<PointD>{new PointD(300, 100), new PointD(400, 100), new PointD(400, 400), new PointD(300, 400)};
            Primitive.Push_Polygon(ref figure2, list2);

            Multi_Figure temp2 = new Multi_Figure();
            temp2.multi_figure.AddRange(figure2);

            all_figures.Add(temp2);

            all_figures[all_figures.Count() - 1].pen.Color = draw_pen.Color;
            all_figures[all_figures.Count() - 2].pen.Color = draw_pen.Color;
            num_selected_figures = 2;
            switch_activity();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            int last = all_figures.Count() - 1;
            int num = all_figures[last].multi_figure.Count();
            List<List<PointD>> figure = new List<List<PointD>>();

            for(int i = 0; i < num; ++ i)
            {
                figure.Clear();
                Primitive.Push_Polygon(ref figure, all_figures[last].multi_figure[i]);
                Multi_Figure temp = new Multi_Figure();
                temp.multi_figure.AddRange(figure);
                all_figures.Add(temp);
            }
            all_figures.RemoveAt(last);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            
            ArPoints.Add(new Point(517, 437)); // 0
            ArPoints.Add(new Point(792, 391)); // 1
            ArPoints.Add(new Point(645, 296)); // 2
            ArPoints.Add(new Point(666, 487)); // 3
            ArPoints.Add(new Point(459, 296)); // 4
            ArPoints.Add(new Point(747, 269)); // 5
            ArPoints.Add(new Point(480, 421)); // 6
            ArPoints.Add(new Point(566, 388)); // 7
            ArPoints.Add(new Point(665, 230)); // 8
            ArPoints.Add(new Point(605, 459)); // 9
            
            
            ArPoints.Add(new Point(546, 400)); // 10
            ArPoints.Add(new Point(715, 171)); // 11
            ArPoints.Add(new Point(508, 433)); // 12
            ArPoints.Add(new Point(509, 248)); // 13
            ArPoints.Add(new Point(724, 476)); // 14
            ArPoints.Add(new Point(514, 225)); // 15
            ArPoints.Add(new Point(731, 128)); // 16
            ArPoints.Add(new Point(535, 465)); // 17
            ArPoints.Add(new Point(610, 260)); // 18
            ArPoints.Add(new Point(627, 311)); // 19
            
            
            ArPoints.Add(new Point(681, 388)); // 20
            ArPoints.Add(new Point(790, 150)); // 21
            ArPoints.Add(new Point(505, 405)); // 22
            ArPoints.Add(new Point(637, 338)); // 23
            ArPoints.Add(new Point(711, 476)); // 24
            ArPoints.Add(new Point(531, 249)); // 25
            ArPoints.Add(new Point(737, 364)); // 26
            ArPoints.Add(new Point(758, 309)); // 27
            ArPoints.Add(new Point(531, 238)); // 28
            ArPoints.Add(new Point(606, 447)); // 29
            ArPoints.Add(new Point(478, 240)); // 30
            
            
            ArPoints.Add(new Point(565, 496)); // 31
            ArPoints.Add(new Point(634, 233)); // 32
            ArPoints.Add(new Point(503, 396)); // 33
            ArPoints.Add(new Point(508, 387)); // 34
            ArPoints.Add(new Point(794, 330)); // 35
            ArPoints.Add(new Point(480, 329)); // 36
            ArPoints.Add(new Point(569, 371)); // 37
            ArPoints.Add(new Point(622, 297)); // 38
            ArPoints.Add(new Point(652, 315)); // 39
            
            
            ArPoints.Add(new Point(615, 386)); // 40
            ArPoints.Add(new Point(636, 278)); // 41
            ArPoints.Add(new Point(644, 422)); // 42
            ArPoints.Add(new Point(581, 261)); // 43
            ArPoints.Add(new Point(581, 241)); // 44
            ArPoints.Add(new Point(641, 319)); // 45
            ArPoints.Add(new Point(515, 304)); // 46
            ArPoints.Add(new Point(661, 293)); // 47
            ArPoints.Add(new Point(563, 356)); // 48
            ArPoints.Add(new Point(629, 255)); // 49
            
            
            ArPoints.Add(new Point(621, 427)); // 50
            ArPoints.Add(new Point(647, 233)); // 51
            ArPoints.Add(new Point(634, 407)); // 52
            ArPoints.Add(new Point(537, 215)); // 53
            ArPoints.Add(new Point(746, 392)); // 54
            ArPoints.Add(new Point(542, 302)); // 55
            ArPoints.Add(new Point(517, 423)); // 56
            ArPoints.Add(new Point(698, 222)); // 57
            ArPoints.Add(new Point(646, 462)); // 58
            ArPoints.Add(new Point(556, 275)); // 59
            
            
            ArPoints.Add(new Point(802, 390)); // 60
            ArPoints.Add(new Point(526, 258)); // 61
            ArPoints.Add(new Point(562, 482)); // 62
            ArPoints.Add(new Point(658, 200)); // 63
            ArPoints.Add(new Point(630, 470)); // 64
            ArPoints.Add(new Point(639, 277)); // 65
            ArPoints.Add(new Point(647, 438)); // 66
            ArPoints.Add(new Point(522, 332)); // 67
            ArPoints.Add(new Point(714, 288)); // 68
            ArPoints.Add(new Point(570, 412)); // 69
            
            
            ArPoints.Add(new Point(671, 331)); // 70
            ArPoints.Add(new Point(695, 267)); // 71
            ArPoints.Add(new Point(464, 347)); // 72
            ArPoints.Add(new Point(582, 211)); // 73
            ArPoints.Add(new Point(655, 432)); // 74
            ArPoints.Add(new Point(580, 231)); // 75
            ArPoints.Add(new Point(654, 304)); // 76
            ArPoints.Add(new Point(489, 342)); // 77
            ArPoints.Add(new Point(751, 327)); // 78
            ArPoints.Add(new Point(582, 354)); // 79
            
            
            ArPoints.Add(new Point(712, 269)); // 80
            ArPoints.Add(new Point(552, 421)); // 81
            ArPoints.Add(new Point(553, 213)); // 82
            ArPoints.Add(new Point(720, 425)); // 83
            ArPoints.Add(new Point(484, 236)); // 84
            ArPoints.Add(new Point(783, 418)); // 85
            ArPoints.Add(new Point(524, 460)); // 86
            ArPoints.Add(new Point(748, 194)); // 87
            ArPoints.Add(new Point(610, 423)); // 88
            ArPoints.Add(new Point(675, 224)); // 89
            
            
            ArPoints.Add(new Point(595, 388)); // 90
            ArPoints.Add(new Point(654, 214)); // 91
            ArPoints.Add(new Point(617, 481)); // 92
            ArPoints.Add(new Point(602, 214)); // 93
            ArPoints.Add(new Point(697, 399)); // 94
            ArPoints.Add(new Point(456, 210)); // 95
            ArPoints.Add(new Point(635, 378)); // 96
            ArPoints.Add(new Point(666, 233)); // 97
            ArPoints.Add(new Point(749, 369)); // 98
            ArPoints.Add(new Point(500, 379)); // 99
        }
    }
}