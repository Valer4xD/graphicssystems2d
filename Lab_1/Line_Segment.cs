﻿using System;
using System.Collections.Generic;

using System.Drawing;
using System.Linq;

using System.IO;

namespace Lab_1 {
    [Serializable]
    public class Line_Segment : Primitive
    {
		const double BORDER = 10.0,
					 CORRECTION_ZOOM = 1000.0;
		const bool LAST_PIXEL = true;

		public PointD Get_Point_A() {
            return Get_Point(true);
		}
		public PointD Get_Point_B() {
            return Get_Point(false);
		}
		public PointD Get_Point(bool first)
		{
			PointD P = new PointD();
			double half_len = STO.get_distance(skelet[0], skelet[1]) / 2.0;
			double shorten = half_len - (half_len / CORRECTION_ZOOM);
			double dX = skelet[1].X - skelet[0].X,
				   dY = skelet[1].Y - skelet[0].Y,
				   hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			P.X = skelet[first ? 0 : 1].X - shorten / hypotenuse * dX * (first ? -1.0 : 1.0);
			P.Y = skelet[first ? 0 : 1].Y - shorten / hypotenuse * dY * (first ? -1.0 : 1.0);

			return P;
		}

        public override void Push(List<Point> polygon) {
            skelet.InsertRange(0, new List<PointD>() { new PointD(polygon[0]), new PointD(polygon[1]) });
            Correction_Zoom();
		}
        public override void Push(List<PointD> polygon) {
			skelet.InsertRange(0, new List<PointD>() { new PointD(polygon[0]), new PointD(polygon[1]) });
            Correction_Zoom();
		}

		public void Correction_Zoom()
		{
			double half_len = STO.get_distance(skelet[0], skelet[1]) / 2.0;
			double extend = half_len * CORRECTION_ZOOM - half_len;
			double dX = skelet[1].X - skelet[0].X,
				   dY = skelet[1].Y - skelet[0].Y,
				   hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			skelet[0].X += extend / hypotenuse * -dX;
			skelet[0].Y += extend / hypotenuse * -dY;
			skelet[1].X += extend / hypotenuse *  dX;
			skelet[1].Y += extend / hypotenuse *  dY;
		}

		public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
            PointD A = Get_Point_A();
            PointD B = Get_Point_B();

            MyDraw.Line(g, draw_pen, screen_height, true, A, B, LAST_PIXEL);

			if(test)
				Paint_Points(g, draw_pen, screen_height, true);
		}

        protected void Paint_Points(Graphics g, Pen draw_pen, int screen_height, bool solid)
		{
			Point center = new Point();
			double radius = draw_pen.Width / 2.0 + 2.0;

			center = MyDraw.PointD_to_Point(Get_Point_A());
            MyDraw.Circle(g, draw_pen, screen_height, solid, center, radius);

			center = MyDraw.PointD_to_Point(Get_Point_B());
            MyDraw.Circle(g, draw_pen, screen_height, solid, center, radius);
		}

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{
            PointD A = Get_Point_A();
            PointD B = Get_Point_B();

            if((int)MyDraw.Double_Round(select_pen.Width) <= 1)
			{
				List<PointD> polygon = new List<PointD>();
				polygon.AddRange(MyDraw.Get_Select_Borders(A, B, pen.Width, BORDER + 0.5, LAST_PIXEL));
                MyDraw.Paint_Single_Frame(g, select_pen, screen_height, polygon, true);
			}
			else
			{
				List<List<PointD>> figure = new List<List<PointD>>();
                figure.Add(MyDraw.Get_Select_Borders(A, B, pen.Width, BORDER, LAST_PIXEL));
                figure.Add(MyDraw.Get_Select_Borders(A, B, pen.Width, BORDER + select_pen.Width, LAST_PIXEL));
				MyDraw.Paint_Bulk_Frame(g, select_pen, screen_height, figure);
			}
		}

		public override bool Is_Include(double X, double Y)
		{
            PointD A = Get_Point_A();
            PointD B = Get_Point_B();

			List<PointD> polygon = MyDraw.Get_Select_Borders(A, B, pen.Width, BORDER, LAST_PIXEL);

			if(Is_Include_Poligon(polygon, X, Y))
				return true;

			return false;
		}

		public override PointD Get_Center()
        {
			PointD center = new PointD();
			center.X = (skelet[1].X - skelet[0].X) / 2.0 + skelet[0].X;
			center.Y = (skelet[1].Y - skelet[0].Y) / 2.0 + skelet[0].Y;
			return center;
        }

// Теоретико-множественные операции --------------------------------------------------------------------------------------------------------------------------------------

		public override void Convert()
		{
			PointD temp_A = new PointD();
			PointD temp_B = new PointD();
			List<PointD> polygon = new List<PointD>();
            List<PointD> line = Get_Line((int)Math.Round(skelet[0].X, MidpointRounding.AwayFromZero),
										 (int)Math.Round(skelet[0].Y, MidpointRounding.AwayFromZero),
										 (int)Math.Round(skelet[1].X, MidpointRounding.AwayFromZero),
										 (int)Math.Round(skelet[1].Y, MidpointRounding.AwayFromZero));
			int last = line.Count() - 1;
			double dX = 0.0,
				   dY = 0.0,
				   hypotenuse = 0.0;

			dX = line[1].X - line[0].X;
			dY = line[1].Y - line[0].Y;
			hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			temp_A.X = line[0].X + 0.5 / hypotenuse * (line[0].X - line[1].X);
			temp_A.Y = line[0].Y + 0.5 / hypotenuse * (line[0].Y - line[1].Y);
			temp_B.Copy(line[1]);
			offset(0.5, ref temp_A, ref temp_B, true);
			polygon.Add(new PointD(temp_A));

			polygon.AddRange(Line_Offset(line, 0.5, true));

			dX = line[last - 1].X - line[last].X;
			dY = line[last - 1].Y - line[last].Y;
			hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			temp_B.X = line[last].X + 0.5 / hypotenuse * (line[last].X - line[last - 1].X);
			temp_B.Y = line[last].Y + 0.5 / hypotenuse * (line[last].Y - line[last - 1].Y);
			temp_A.Copy(line[last - 1]);
			offset(0.5, ref temp_A, ref temp_B, true);
			polygon.Add(new PointD(temp_A));

			polygon.Reverse();

			dX = line[1].X - line[0].X;
			dY = line[1].Y - line[0].Y;
			hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			temp_A.X = line[0].X + 0.5 / hypotenuse * (line[0].X - line[1].X);
			temp_A.Y = line[0].Y + 0.5 / hypotenuse * (line[0].Y - line[1].Y);
			temp_B.Copy(line[1]);
			offset(0.5, ref temp_A, ref temp_B, false);
			polygon.Add(new PointD(temp_A));

			polygon.AddRange(Line_Offset(line, 0.5, false));

			dX = line[last - 1].X - line[last].X;
			dY = line[last - 1].Y - line[last].Y;
			hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			temp_B.X = line[last].X + 0.5 / hypotenuse * (line[last].X - line[last - 1].X);
			temp_B.Y = line[last].Y + 0.5 / hypotenuse * (line[last].Y - line[last - 1].Y);
			temp_A.Copy(line[last - 1]);
			offset(0.5, ref temp_A, ref temp_B, false);
			polygon.Add(new PointD(temp_A));

			if(!STO.clockwise_direction(polygon))
				polygon.Reverse();

			multi_figure.Clear();
			multi_figure.Add(polygon);
		}

        public List<PointD> Get_Line(int x1, int y1, int x2, int y2)
        {
            List<PointD> line = new List<PointD>();

			int x = x1,
				y = y1,
				F  = 0,
				Fx = 0,
				Fy = 0,
				dx = x2 - x1,
				dy = y2 - y1,
				Sx = Math.Sign(dx),
				Sy = Math.Sign(dy),
				dFx = (Sx > 0) ? dy : -dy,
				dFy = (Sy > 0) ? dx : -dx;

			bool tr = true;

            line.Add(new PointD(x1, y1));
            if(Math.Abs(dx) >= Math.Abs(dy))	// угол наклона <= 45 градусов
                do {
                    if(x == x2)		break;

                    Fx = F  + dFx;
                    F  = Fx - dFy;
                    x  = x  + Sx;

                    if(Math.Abs(Fx) < Math.Abs(F))  F = Fx;
                    else
					{
                        line.Add(new PointD(x, y));
						y = y + Sy;
                        line.Add(new PointD(x, y));
					}
                }
                while(tr);
            else								// угол наклона > 45 градусов
                do {
                    if(y == y2)		break;

                    Fy = F  + dFy;
                    F  = Fy - dFx;
                    y  = y  + Sy;

                    if(Math.Abs(Fy) < Math.Abs(F))  F = Fy;
                    else
					{
                        line.Add(new PointD(x, y));
						x = x + Sx;
                        line.Add(new PointD(x, y));
					}
                }
                while(tr);
			if(x != x2 || y != y2)
				line.Add(new PointD(x2, y2));

            return line;
        }

        private List<PointD> Line_Offset(List<PointD> line, double border, bool direction)
		{
			List<PointD> result_line = new List<PointD>();

			PointD R = new PointD();
			PointD A1 = new PointD();
			PointD B1 = new PointD();
			PointD A2 = new PointD();
			PointD B2 = new PointD();

			int num_vertexs = line.Count() - 1;
			for(int i = 0; i <= num_vertexs - 2; ++ i)
			{
				A1.Copy(line[i]);
				A2.Copy(line[i + 1]);
				B1.Copy(line[i + 1]);
				B2.Copy(line[i + 2]);

				offset(border, ref A1, ref A2, direction);
				offset(border, ref B1, ref B2, direction);

				STO.is_intersection(A1, A2, B1, B2, ref R);

				result_line.Add(new PointD(R));
			}

            return result_line;
		}
	}
}