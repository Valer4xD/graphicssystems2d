﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Drawing;

namespace Lab_1 {
    [Serializable]
    public class Bezier_Сurve : Line_Blank
    {
        public override void Push(List<PointD> polygon)
        {
			double left   = polygon[0].X;
			double right  = polygon[0].X;
			double top    = polygon[0].Y;
			double bottom = polygon[0].Y;

			multi_figure.Add(new List<PointD>());
			multi_figure[0].Add(new PointD(left, top));

			int n = polygon.Count() - 1;

            double		 nFact = Factorial(n);
            const double dt    = 0.01;
            double		 t     = dt;

            double xt = 0,
				   yt = 0;

            int    i = 0;
            double J = 0;

            while (t < 1 + dt / 2)
            {
                i = 0;

                xt = 0;
                yt = 0;

                while (i <= n)
                {
                    J = Math.Pow(t, i) * Math.Pow(1 - t, n - i) * nFact / (Factorial(i) * Factorial(n - i));
                    xt += polygon[i].X * J;
                    yt += polygon[i].Y * J;
                    ++ i;
                }

                t += dt;

				if(xt < left)		left   = xt;
				if(xt > right)		right  = xt;
				if(yt < top)		top	   = yt;
				if(yt > bottom)		bottom = yt;

				multi_figure[0].Add(new PointD(xt, yt));
            }

			left   -= 15;
			right  += 15;
			top    -= 15;
			bottom += 15;

			List<PointD> selection_area = new List<PointD>();
			selection_area.Add(new PointD(left,  top));
			selection_area.Add(new PointD(right, top));
			selection_area.Add(new PointD(right, bottom));
			selection_area.Add(new PointD(left,  bottom));
            Push_Polygon(ref multi_figure, selection_area);
            Push_Polygon(ref multi_figure, polygon);
		}

        private ulong Factorial(int n)
        {
            if(n <  0)				return 0;
            if(n == 0)              return 1;
            if(n == 1 || n == 2)    return (ulong)n;

            return ProdTree(2, (ulong)n);
        }

        private ulong ProdTree(ulong l, ulong r)
        {
            if(l >  r)		return 1;
            if(l == r)      return l;
            if(r - l == 1)  return l * r;

            ulong m = (l + r) / 2;

            return ProdTree(l, m) * ProdTree(m + 1, r);
        }

		public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
			int num_vertexs = multi_figure[0].Count() - 1;
			for(int VC = 0, VN = 1; VC < num_vertexs; ++ VC, ++ VN)
				Paint_Line_Round(g, draw_pen, multi_figure[0][VC], multi_figure[0][VN]);
			if(test)
				Paint_Points(g, draw_pen, num_vertexs);
		}
    }
}