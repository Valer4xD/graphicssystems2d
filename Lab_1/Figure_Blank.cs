﻿using System;
using System.Linq;

using System.Drawing;

namespace Lab_1 {
    [Serializable]
    public class Figure_Blank : Primitive
	{
		public PointD first_point  = new PointD(0, 0);
		public PointD second_point = new PointD(0, 0);

        public virtual void Set_Vertexs() {}

        public void start_creating(double X, double Y)
        {
			first_point  = new PointD(X, Y);
            second_point = new PointD(X, Y);
		}

		public override void update_creating(double X, double Y)
		{
			second_point = new PointD(X, Y);
            Set_Vertexs();
		}

        public override bool end_creating()
        {
			if(first_point.X == second_point.X
			|| first_point.Y == second_point.Y)
				return false;
			return true;
        }

        public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test) {
            if(multi_figure.Count() == 1)
                Paint_Polygon(g, draw_pen, screen_height, multi_figure[0]);
        }

		public override bool Is_Include(double X, double Y)
		{
			if(Is_Include_Poligon(multi_figure[0], X, Y))		return true;
			return false;
		}
	}
}