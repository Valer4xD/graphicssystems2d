﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Windows.Forms;

namespace Lab_1 {
	[Serializable]
    public class Straight_Random : Primitive
	{
		const double BORDER = 10.0;

        public override void Push(List<Point> polygon) {
			skelet.InsertRange(0, new List<PointD>() { new PointD(polygon[0]), new PointD(polygon[1]) });
		}
        public override void Push(List<PointD> polygon) {
			skelet.InsertRange(0, new List<PointD>() { new PointD(polygon[0]), new PointD(polygon[1]) });
		}

		public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
			Paint_Straight(g, draw_pen, screen_width, screen_height, skelet[0], skelet[1]);
		}

		private void Paint_Straight(Graphics g, Pen obj_pen, int width, int height, PointD A, PointD B)
		{
            if((int)Math.Round(obj_pen.Width, MidpointRounding.AwayFromZero) <= 1)
			{
				List<PointD> line = new List<PointD>(); 
				Get_Line(width, height, A, B, ref line);

				if(line.Count() == 2)
                    MyDraw.Line(g, obj_pen, height, true, line[0],
														  line[1], true);
			}
			else
			{
				PointD	bA = new PointD(),
						bB = new PointD();
				List<PointD> first_line_points	= new List<PointD>(),
							 second_line_points	= new List<PointD>(),
							 line_points		= new List<PointD>(),
							 angles_points		= new List<PointD>(),
							 final_polygon		= new List<PointD>();
				List<int> first_line_sides = new List<int>(),
						 second_line_sides = new List<int>(),
						 line_sides		   = new List<int>();
				double half_width = obj_pen.Width / 2;
				int i = 0;

				bA.Copy(A); bB.Copy(B);
				MyDraw.offset(half_width, ref bA, ref bB, true);
				first_line_sides.AddRange(Get_Line(width, height, bA, bB, ref first_line_points));

				bA.Copy(A); bB.Copy(B);
				MyDraw.offset(half_width, ref bA, ref bB, false);
				second_line_sides.AddRange(Get_Line(width, height, bA, bB, ref second_line_points));

				if(first_line_points.Count() == 2 && second_line_points.Count() == 2)
				{
					first_line_points.Reverse();
					first_line_sides.Reverse();

					final_polygon.AddRange(first_line_points);
					if(first_line_sides[0] == second_line_sides[1]
					&& first_line_sides[1] == second_line_sides[0])
						final_polygon.AddRange(second_line_points);
					else
					{
						if(first_line_sides[1] != second_line_sides[0])
							final_polygon.Add(switch_angle(first_line_sides[1], second_line_sides[0], width, height));

						final_polygon.AddRange(second_line_points);

						if(second_line_sides[1] != first_line_sides[0])
							final_polygon.Add(switch_angle(second_line_sides[1], first_line_sides[0], width, height));
					}
				}
				else
				{
						 if(first_line_points.Count()  == 2)		{
							 line_points.AddRange(first_line_points);
							 line_sides.AddRange(first_line_sides);	}
					else if(second_line_points.Count() == 2)		{
							 line_points.AddRange(second_line_points);
							 line_sides.AddRange(second_line_sides);}

                    if(line_sides.Count() == 2)
                    {
					    angles_points.Add(new PointD(0,		0));
					    angles_points.Add(new PointD(width,	0));
					    angles_points.Add(new PointD(width,	height));
					    angles_points.Add(new PointD(0,		height));

					    for(i = 0; i < 4; ++ i)
					    {
						    if(Distance_Point_Straight(A, B, angles_points[i]) <= half_width)   final_polygon.Add(angles_points[i]);
						    if(line_sides[0] == i + 1)											final_polygon.Add(line_points[0]);
						    if(line_sides[1] == i + 1)											final_polygon.Add(line_points[1]);
					    }
                    }
				}

				MyDraw.Paint_Polygon(g, obj_pen, height, final_polygon);
			}
		}

		private List<int> Get_Line(int width, int height, PointD A, PointD B, ref List<PointD> line)
		{
			double	dX = B.X - A.X,
					dY = B.Y - A.Y;
			PointD	swap = new PointD(),
					T	 = new PointD();
			List<int> sides = new List<int>();

			if(dX >  0 && dY <  0
			|| dX <  0 && dY <  0
			|| dX == 0 && dY >  0
			|| dX >  0 && dY == 0)
			{
				swap.Copy(A);
				A.Copy(B);
				B.Copy(swap);

				dX *= -1;
				dY *= -1;
			}

			if(dX > 0 && dY > 0)
			{
				T.Copy(A);
				Get_Point(ref T, B, 0, -1,		dX, dY, ref sides, 4, 1); // -1 вместо 0 - не закрашивалась верхняя строчка
				Check_Point(T, width, height, ref line);

				T.Copy(A);
				Get_Point(ref T, B, width,	height,	dX, dY, ref sides, 2, 3);
				Check_Point(T, width, height, ref line);
			}
			else if(dX < 0 && dY > 0)
			{
				T.Copy(A);
				Get_Point(ref T, B, width, -1,	dX, dY, ref sides, 2, 1); // -1 вместо 0 - не закрашивалась верхняя строчка
				Check_Point(T, width, height, ref line);

				T.Copy(A);
				Get_Point(ref T, B, 0, height,	dX, dY, ref sides, 4, 3);
				Check_Point(T, width, height, ref line);
			}

			else if(dX == 0 && dY != 0)
			{
				Check_Point(T.Copy(A.X, 0),      width, height, ref line);
				Check_Point(T.Copy(A.X, height), width, height, ref line);

				sides.Add(1);
				sides.Add(3);
			}
			else if(dX != 0 && dY == 0)
			{
                Check_Point(T.Copy(0,	  A.Y), width, height, ref line);
                Check_Point(T.Copy(width, A.Y), width, height, ref line);

				sides.Add(2);
				sides.Add(4);
			}

			return sides;
		}

		private void Get_Point(ref PointD A, PointD B, int width, int height, double dX, double dY, ref List<int> sides, int V1, int V2)
		{
			double	X1 = width - A.X,
					Y1 = height - A.Y,
					X2 = Y1 / dY * dX,
					Y2 = X1 / dX * dY;

			if(height == -1 && A.Y + Y2 > A.Y + Y1	// -1 вместо 0 - не закрашивалась верхняя строчка
			|| height != -1 && A.Y + Y2 < A.Y + Y1)
			{
				A.X += X1;
				A.Y += Y2;
				sides.Add(V1);
			}
			else
			{
				A.X += X2;
				A.Y += Y1;
				sides.Add(V2);
			}
		}

		private void Check_Point(PointD T, int width, int height, ref List<PointD> line)
		{
			const double EPS = 0.02; // Макс. погр. ICC = 0.01
			if(T.X > 0 - EPS && T.X < width + EPS && T.Y > 0 - EPS - 1 && T.Y < height + EPS) // -1 добавлена - не закрашивалась верхняя строчка
				line.Add(new PointD(T));
		}

		private PointD switch_angle(int first, int second, int width, int height)
		{
			switch(first)
			{
				case 1:	return	(second == 2) ?  new PointD(width,	0)		:	new PointD(0,		0);
				case 2:	return	(second == 3) ?  new PointD(width,	height)	:	new PointD(width,	0);
				case 3:	return	(second == 4) ?  new PointD(0,		height)	:	new PointD(width,	height);
				case 4:	return	(second == 1) ?  new PointD(0,		0)		:	new PointD(0,		height);
			}

			return new PointD();
		}

		private double Distance_Point_Straight(PointD A, PointD B, PointD CP)
		{
			double dX = B.X - A.X,
				   dY = B.Y - A.Y;
			return Math.Abs((dX * (CP.Y - A.Y) - dY * (CP.X - A.X)) / Math.Sqrt(dX * dX + dY * dY));
		}

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{
			double border = BORDER + (select_pen.Width + pen.Width) / 2.0; // 0.5 добавлен из-за округления

			PointD A = new PointD();
			PointD B = new PointD();

            A.Copy(skelet[0]);
			B.Copy(skelet[1]);
			if(MyDraw.offset(border, ref A, ref B, true))
			    Paint_Straight(g, select_pen, screen_width, screen_height, A, B);

            A.Copy(skelet[0]);
			B.Copy(skelet[1]);
			if(MyDraw.offset(border, ref A, ref B, false))
			    Paint_Straight(g, select_pen, screen_width, screen_height, A, B);
        }

		public override bool Is_Include(double X, double Y)
		{
			if(Distance_Point_Straight(skelet[0], skelet[1], new PointD(X, Y)) <= BORDER)
				return true;
			return false;
		}

		public override PointD Get_Center()
        {
			PointD center = new PointD();
			center.X = (skelet[1].X - skelet[0].X) / 2 + skelet[0].X;
			center.Y = (skelet[1].Y - skelet[0].Y) / 2 + skelet[0].Y;

			return center;
        }
	}
}