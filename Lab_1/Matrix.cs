﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab_1 {
    public class Matrix {
        public static List<List<double>> multiplication(ref List<List<double>> a,  List<List<double>> b)
        {
            List<List<double>> result = new List<List<double>>();

            int height_a = a.Count(),
				height_b = b.Count(),
				width_a = 0,
				width_b = 0,
				ha,
				hb,
				wa,
				wb;
			if(height_a > 0)	width_a = a[0].Count();
			if(height_b > 0)	width_b = b[0].Count();

			if(width_a == height_b)
			{
				for(ha = 0; ha < height_a; ++ ha)
					if(a[ha].Count() != width_a)		return	result;

				for(hb = 0; hb < height_b; ++ hb)
					if(b[hb].Count() != width_b)		return	result;

				for(ha = 0; ha < height_a; ++ ha)
					result.Add(new List<double>());

				for(wb = 0; wb < width_b; ++ wb)
					for(ha = 0; ha < height_a; ++ ha)
					{
						result[ha].Add(0.0);
						for(wa = 0; wa < width_a; ++ wa)
							result[ha][wb] += a[ha][wa] * b[wa][wb];
					}
			}

            a = result;
            return result;
        }
    }
}
