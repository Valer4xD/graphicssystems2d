﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Drawing;

namespace Lab_1 {
    [Serializable]
    public class Arrow : Figure_Blank
	{
        public override void Set_Vertexs()
        {
			List<PointD> polygon = new List<PointD>();
			if(multi_figure.Count() > 0)    multi_figure.RemoveAt(0);

			if(first_point.X < second_point.X)
			{
				if(first_point.Y > second_point.Y)
				{
					polygon.Add(new PointD(first_point.X, second_point.Y));
					polygon.Add(new PointD((second_point.X - first_point.X) / 4 * 3 + first_point.X, second_point.Y));
					polygon.Add(new PointD(second_point.X, (first_point.Y - second_point.Y) / 2 + second_point.Y));
					polygon.Add(new PointD((second_point.X - first_point.X) / 4 * 3 + first_point.X, first_point.Y));
					polygon.Add(new PointD(first_point.X, first_point.Y));
					polygon.Add(new PointD((second_point.X - first_point.X) / 4 * 1 + first_point.X, (first_point.Y - second_point.Y) / 2 + second_point.Y));
				}
                if(first_point.Y < second_point.Y)
                {
					polygon.Add(new PointD(first_point.X, first_point.Y));
					polygon.Add(new PointD((second_point.X - first_point.X) / 4 * 3 + first_point.X, first_point.Y));
					polygon.Add(new PointD(second_point.X, (first_point.Y - second_point.Y) / 2 + second_point.Y));
					polygon.Add(new PointD((second_point.X - first_point.X) / 4 * 3 + first_point.X, second_point.Y));
					polygon.Add(new PointD(first_point.X, second_point.Y));
					polygon.Add(new PointD((second_point.X - first_point.X) / 4 * 1 + first_point.X, (second_point.Y - first_point.Y) / 2 + first_point.Y));
				}
			}
			if(first_point.X > second_point.X)
			{
				if(first_point.Y < second_point.Y)
				{
					polygon.Add(new PointD(second_point.X, first_point.Y));
					polygon.Add(new PointD((first_point.X - second_point.X) / 4 * 3 + second_point.X, first_point.Y));
					polygon.Add(new PointD(first_point.X, (second_point.Y - first_point.Y) / 2 + first_point.Y));
					polygon.Add(new PointD((first_point.X - second_point.X) / 4 * 3 + second_point.X, second_point.Y));
					polygon.Add(new PointD(second_point.X, second_point.Y));
					polygon.Add(new PointD((first_point.X - second_point.X) / 4 * 1 + second_point.X, (second_point.Y - first_point.Y) / 2 + first_point.Y));
				}
				if(first_point.Y > second_point.Y)
				{
					polygon.Add(new PointD(second_point.X, second_point.Y));
					polygon.Add(new PointD((first_point.X - second_point.X) / 4 * 3 + second_point.X, second_point.Y));
					polygon.Add(new PointD(first_point.X, (first_point.Y - second_point.Y) / 2 + second_point.Y));
					polygon.Add(new PointD((first_point.X - second_point.X) / 4 * 3 + second_point.X, first_point.Y));
					polygon.Add(new PointD(second_point.X, first_point.Y));
					polygon.Add(new PointD((first_point.X - second_point.X) / 4 * 1 + second_point.X, (first_point.Y - second_point.Y) / 2 + second_point.Y));
				}
			}
			multi_figure.Insert(0, polygon);
        }

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{
			/*
			Paint_Line_Round(g, select_pen, multi_figure[0][0], multi_figure[0][1]);
			Paint_Line_Round(g, select_pen, multi_figure[0][1], multi_figure[0][2]);
			Paint_Line_Round(g, select_pen, multi_figure[0][2], multi_figure[0][3]);
			Paint_Line_Round(g, select_pen, multi_figure[0][3], multi_figure[0][4]);
			Paint_Line_Round(g, select_pen, multi_figure[0][4], multi_figure[0][5]);
			Paint_Line_Round(g, select_pen, multi_figure[0][5], multi_figure[0][0]);
			*/
			/*
			Draw.Line(g, select_pen, false, multi_figure[0][0], multi_figure[0][1]);
            Draw.Line(g, select_pen, false, multi_figure[0][1], multi_figure[0][2]);
            Draw.Line(g, select_pen, false, multi_figure[0][2], multi_figure[0][3]);
            Draw.Line(g, select_pen, false, multi_figure[0][3], multi_figure[0][4]);
            Draw.Line(g, select_pen, false, multi_figure[0][4], multi_figure[0][5]);
            Draw.Line(g, select_pen, false, multi_figure[0][5], multi_figure[0][0]);
			*/
			
			List<List<PointD>> figure = new List<List<PointD>>();

			figure.Add(polygon_resize(multi_figure[0], select_pen.Width / 2, true));
			figure.Add(polygon_resize(multi_figure[0], select_pen.Width / 2, false));

			float backup = select_pen.Width;
			select_pen.Width = 1;

			List<int> Xb = new List<int>();

			for(int Y = 0; Y <= screen_height; ++ Y)
			{
				Xb.Clear();

				Xb.AddRange(Get_Borders(figure[0], Y));
				Xb.AddRange(Get_Borders(figure[1], Y));

				Xb.Sort();

				int borders = Xb.Count() - 1;
				for(int b = 0; b < borders; b += 2)
					g.DrawLine(select_pen, Xb[b],		  Y,
										   Xb[b + 1] - 1, Y); // (- 1) - почему то на 1-цу шире
			}

			select_pen.Width = backup;
			
		}

		public List<PointD> polygon_resize(List<PointD> polygon, double BORDER, bool direction)
		{
			List<PointD> result_polygon = new List<PointD>();

			PointD R = new PointD();
			PointD A1 = new PointD();
			PointD B1 = new PointD();
			PointD A2 = new PointD();
			PointD B2 = new PointD();

			int num_vertexs = polygon.Count() - 1;

			for(int C = 0, P, N; C <= num_vertexs; ++ C)
			{
				if(C == 0)				P = num_vertexs;
				else					P = C - 1;

				if(C < num_vertexs)		N = C + 1;
				else					N = 0;

				A1.Copy(polygon[P]);
				A2.Copy(polygon[C]);
				B1.Copy(polygon[C]);
				B2.Copy(polygon[N]);

				offset(BORDER, ref A1, ref A2, direction);
				offset(BORDER, ref B1, ref B2, direction);

				STO.is_intersection(A1, A2, B1, B2, ref R);

				result_polygon.Add(new PointD(R));
			}

			return result_polygon;
		}
    }
}