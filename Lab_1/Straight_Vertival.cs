﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

namespace Lab_1 {
    [Serializable]
    class Straight_Vertival : Primitive
    {
		const double BORDER = 10.0;

		public override void Push(double X, double Y) {
            skelet.Insert(0, new PointD(X, Y));
		}

		public override void Push(Point P) {
            skelet.Insert(0, new PointD(P));
		}
		public override void Push(PointD P) {
            skelet.Insert(0, new PointD(P));
		}

        public override void Push(List<Point> polygon) {
            skelet.Insert(0, new PointD(polygon[0]));
		}
        public override void Push(List<PointD> polygon) {
            skelet.Insert(0, new PointD(polygon[0]));
		}

		public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
			if(Check_Visibility(draw_pen, screen_height, skelet[0].Y))
				MyDraw.Line(g, draw_pen, screen_height, true, 0,			skelet[0].Y,
                                                              screen_width, skelet[0].Y, true);
		}

		private bool Check_Visibility(Pen obj_pen, int height, double Y)
		{
			const double EPS = 0.02; // Макс. погр. ICC = 0.01
			double half_width = obj_pen.Width / 2;
			if(Y > 0 - EPS - half_width && Y < height + EPS + half_width)
				return true;
			return false;
		}

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{
            double border = BORDER + (select_pen.Width + pen.Width) / 2.0;

			if(Check_Visibility(select_pen, screen_height, skelet[0].Y + BORDER))
				MyDraw.Line(g, select_pen, screen_height, true, 0,			  skelet[0].Y + border,
                                                                screen_width, skelet[0].Y + border, true);

			if(Check_Visibility(select_pen, screen_height, skelet[0].Y - BORDER))
				MyDraw.Line(g, select_pen, screen_height, true, 0,			  skelet[0].Y - border,
                                                                screen_width, skelet[0].Y - border, true);
        }

		public override bool Is_Include(double X, double Y)
		{
			double diff = Y - skelet[0].Y;
			if(Math.Abs(diff) < BORDER)
				return true;
			return false;
		}

		public override PointD Get_Center()
        {
			return skelet[0];
        }
	}
}