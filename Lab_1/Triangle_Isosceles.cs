﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Drawing;

namespace Lab_1 {
    [Serializable]
    public class Triangle_Isosceles : Primitive
    {
		double HALF_WIDTH_TEMPLATE  = 0.0,
			   HALF_HEIGHT_TEMPLATE = 0.0;

		public PointD first_point  = new PointD(0, 0);
		public PointD second_point = new PointD(0, 0);

		public Triangle_Isosceles()
		{
			skelet.Add(new PointD( 50.0,  50.0));
			skelet.Add(new PointD(100.0,  50.0));
			skelet.Add(new PointD( 50.0, 100.0));

            Build_Template();
		}

		public void Build_Template()
		{
			HALF_WIDTH_TEMPLATE  = Math.Abs(skelet[1].X - skelet[0].X);
			HALF_HEIGHT_TEMPLATE = Math.Abs(skelet[2].Y - skelet[0].Y);

			center_template = new PointD(skelet[0]);

            template.Clear();
			template.Add(new List<PointD>());

			template[0].Add(new PointD(skelet[0].X - HALF_WIDTH_TEMPLATE, skelet[2].Y));
			template[0].Add(new PointD(skelet[0].X, skelet[0].Y - HALF_HEIGHT_TEMPLATE));
			template[0].Add(new PointD(skelet[1].X, skelet[2].Y));
		}

        public void start_creating(double X, double Y)
        {
			first_point  = new PointD(X, Y);
            second_point = new PointD(X, Y);
			Set_Vertexs();
		}

		public override void update_creating(double X, double Y)
		{
			second_point = new PointD(X, Y);
            Set_Vertexs();
		}

        public override bool end_creating()
        {
            return true;
        }

        public void Set_Vertexs()
        {
			skelet.Clear();
			skelet.Add(new PointD());
			skelet.Add(new PointD());
			skelet.Add(new PointD());

			const double EPS = 0.02; // Макс. погр. ICC = 0.01
			if(STO.get_distance(first_point, second_point) < EPS)
			{
				first_point  = new PointD(first_point.X - 0.5, first_point.Y - 0.5);
                second_point = new PointD(first_point.X + 1.0, first_point.Y + 1.0);
			}

			skelet[0].X = (second_point.X - first_point.X) / 2 + first_point.X;
			skelet[0].Y = (second_point.Y - first_point.Y) / 2 + first_point.Y;
			skelet[1].X = second_point.X > first_point.X ? second_point.X : first_point.X;
            skelet[1].Y = skelet[0].Y;
			skelet[2].X = skelet[0].X;
			skelet[2].Y = second_point.Y > first_point.Y ? second_point.Y : first_point.Y;

            // Build_Template();
        }

        public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
			if(!MyDraw.Paint_Polygon(g, draw_pen, screen_height, Transformation(template[0])))
			{
				Bitmap pt = new Bitmap(1, 1);
				pt.SetPixel(0, 0, draw_pen.Color);
				PointD center = Get_Center(); 
				g.DrawImageUnscaled(pt, (int)Math.Round(center.X, MidpointRounding.AwayFromZero),
										(int)Math.Round(center.Y, MidpointRounding.AwayFromZero));
			}
		}

		public List<PointD> Transformation(List<PointD> polygon)
		{
			Animation animation = new Animation();

			animation.zoom_priority = 1;
			animation.zoom_delta_X = Get_Half_Width()  / HALF_WIDTH_TEMPLATE;
			animation.zoom_delta_Y = Get_Half_Height() / HALF_HEIGHT_TEMPLATE;

			animation.turn_priority = 2;
			animation.turn_delta = Get_Angle(); // Угол поворота в градусах

			animation.axis_X_turn = animation.axis_X_zoom = center_template.X;
			animation.axis_Y_turn = animation.axis_Y_zoom = center_template.Y;

			animation.move_priority = 3;
			animation.move_delta_X = skelet[0].X - center_template.X;
			animation.move_delta_Y = skelet[0].Y - center_template.Y;

			List<PointD> result_polygon = Copy_Polygon(polygon);
			Polygon_Transformation(result_polygon, animation);

			return result_polygon;
		}

		public double Get_Half_Width() {
			return STO.get_distance(skelet[0], skelet[1]);;
		}
		public double Get_Half_Height() {
			return STO.get_distance(skelet[0], skelet[2]);
		}

		public double Get_Angle()
		{
			double dX = skelet[2].X - skelet[0].X,
				   dY = skelet[2].Y - skelet[0].Y;

			return -Math.Atan2(dX, dY) / Math.PI * 180;
		}

        public List<PointD> Copy_Polygon(List<PointD> out_polygon)
		{
			List<PointD> in_polygon = new List<PointD>();
			int num_vertexs = out_polygon.Count();
			for(int V = 0; V < num_vertexs; ++ V)
				in_polygon.Add(new PointD(out_polygon[V]));
			return in_polygon;
		}

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{/*
			if((int)Math.Round(select_pen.Width, MidpointRounding.AwayFromZero) <= 1)
                MyDraw.Paint_Single_Frame(g, select_pen, screen_height, Transformation(template[0]), true);
			else
			{
				List<PointD> polygon = Transformation(template[0]);
				List<PointD> out_polygon = MyDraw.polygon_resize(polygon, select_pen.Width / 2.0, true);
				List<PointD> in_polygon  = MyDraw.polygon_resize(polygon, select_pen.Width / 2.0, false);

				if(Check_Width_Borders(out_polygon, in_polygon, select_pen.Width))
				{
					List<List<PointD>> figure = new List<List<PointD>>();
					figure.Add(out_polygon);
					figure.Add(in_polygon);
					MyDraw.Paint_Bulk_Frame(g, select_pen, screen_height, figure);
				}
				else
                    MyDraw.Paint_Polygon(g, select_pen, screen_height, out_polygon);
			}*/
		}

		public override bool Is_Include(double X, double Y)
		{
			if(Is_Include_Poligon(Transformation(template[0]), X, Y))
				return true;
			return false;
		}

		public override PointD Get_Center()
		{
			return skelet[0];
		}
    }
}