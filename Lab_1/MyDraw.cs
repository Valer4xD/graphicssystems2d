﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

using System.IO;

namespace Lab_1
{
    public class MyDraw
    {
		public static double Double_Round(double num)
		{
            const double EPS = 0.000001;
			return (int)(num + (num >= 0.0 ? 0.5 + EPS : -(0.5 + EPS)));
		}

		public static PointD PointD_Round(PointD P)
		{
			return new PointD(Double_Round(P.X),
							  Double_Round(P.Y));
		}

		public static Point PointD_to_Point(PointD P)
		{
			return new Point((int)Double_Round(P.X),
							 (int)Double_Round(P.Y));
		}

        public static void Line(Graphics g, Pen obj_pen, int height, bool solid, double x1, double y1, double x2, double y2, bool last_pixel)
        {
            Line(g, obj_pen, height, solid, new PointD(x1, y1), new PointD(x2, y2), last_pixel);
		}

        public static void Line(Graphics g, Pen obj_pen, int height, bool solid, Point A, Point B, bool last_pixel)
        {
            Line(g, obj_pen, height, solid, new PointD(A), new PointD(B), last_pixel);
		}

        public static void Line(Graphics g, Pen obj_pen, int height, bool solid, PointD A, PointD B, bool last_pixel)
        {
            if((int)Math.Round(obj_pen.Width, MidpointRounding.AwayFromZero) == 1)
                Line_Single(g, obj_pen, height, solid, PointD_to_Point(A),
													   PointD_to_Point(B), last_pixel);
            else
            {
                List<PointD> bulk_line = Get_Select_Borders(A, B, obj_pen.Width, 0.0, last_pixel);
				Paint_Polygon(g, obj_pen, height, bulk_line);
            }
		}

		public static List<PointD> Get_Select_Borders(PointD A, PointD B, double width_line, double border, bool last_pixel)
		{
			List<PointD> result_polygon = new List<PointD>();

			PointD extended_A = new PointD(A),
				   extended_B = new PointD(B),
				   temp_A = new PointD(),
				   temp_B = new PointD();

			double dX = B.X - A.X,
				   dY = B.Y - A.Y,
				   hypotenuse = Math.Sqrt(dX * dX + dY * dY);

			if(hypotenuse != 0.0)
			{
				double pixel = last_pixel ? 0.0 : 1.0;

				extended_A.X += (border + 0.5)		   / hypotenuse * -dX;
				extended_A.Y += (border + 0.5)		   / hypotenuse * -dY;
				extended_B.X += (border + 0.5 - pixel) / hypotenuse *  dX;
				extended_B.Y += (border + 0.5 - pixel) / hypotenuse *  dY;
			}

			temp_A.Copy(extended_A);
			temp_B.Copy(extended_B);
			offset(border + width_line / 2.0, ref temp_A, ref temp_B, true);
            result_polygon.Add(new PointD(temp_A));
            result_polygon.Add(new PointD(temp_B));

            temp_A.Copy(extended_A);
			temp_B.Copy(extended_B);
			offset(border + width_line / 2.0, ref temp_A, ref temp_B, false);
            result_polygon.Add(new PointD(temp_B));
            result_polygon.Add(new PointD(temp_A));

			return result_polygon;
		}

        public static void Line_Single(Graphics g, Pen obj_pen, int height, bool solid, int x1, int y1, int x2, int y2, bool last_pixel)
        {
            Line_Single(g, obj_pen, height, solid, new Point(x1, y1), new Point(x2, y2), last_pixel);
		}

        public static void Line_Single(Graphics g, Pen obj_pen, int height, bool solid, Point A, Point B, bool last_pixel)
        {
			Bitmap pt = new Bitmap(1, 1);
			pt.SetPixel(0, 0, obj_pen.Color);

            int x = A.X,
                y = A.Y,
                dx = B.X - A.X,
                dy = B.Y - A.Y,
                Sx = Math.Sign(dx),
                Sy = Math.Sign(dy);

            int F  = 0,
                Fx = 0,
				Fy = 0,
                dFx = (Sx > 0) ? dy : -dy,
				dFy = (Sy > 0) ? dx : -dx;

            if(Math.Abs(dx) >= Math.Abs(dy))	// угол наклона <= 45 градусов
			{
                do {
					g.DrawImageUnscaled(pt, x, y); // Point(g, obj_pen, height, x, y);

                    Fx = F  + dFx;
                    F  = Fx - dFy;
                    x  = x  + Sx;

                    if(Math.Abs(Fx) < Math.Abs(F))  F = Fx;
                    else
					{
						if(solid)
							g.DrawImageUnscaled(pt, x, y); // Point(g, obj_pen, height, x, y);
						y = y + Sy;
					}
                }
                while(x != B.X);
				if(last_pixel)
					g.DrawImageUnscaled(pt, x, y); // Point(g, obj_pen, height, x, y);
			}
            else								// угол наклона > 45 градусов
			{
                do {
					g.DrawImageUnscaled(pt, x, y); // Point(g, obj_pen, height, x, y);

                    Fy = F  + dFy;
                    F  = Fy - dFx;
                    y  = y  + Sy;

                    if(Math.Abs(Fy) < Math.Abs(F))  F = Fy;
                    else
					{
						if(solid)
							g.DrawImageUnscaled(pt, x, y); // Point(g, obj_pen, height, x, y);
						x = x + Sx;
					}
                }
                while(y != B.Y);
				if(last_pixel)
					g.DrawImageUnscaled(pt, x, y); // Point(g, obj_pen, height, x, y);
			}
        }

        public static void Point(Graphics g, Pen obj_pen, int height, int X, int Y)
        {
			Point center = new Point(X, Y);
			double radius = obj_pen.Width / 2;
            Circle(g, obj_pen, height, true, center, radius);
        }

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public static bool Paint_Polygon(Graphics g, Pen obj_pen, int height, List<PointD> polygon)
		{
			float backup = obj_pen.Width;
			obj_pen.Width = 1.0F;

			bool draw = false;

			int len = 0;
			Bitmap pt = new Bitmap(1, 1);
			pt.SetPixel(0, 0, obj_pen.Color);

			List<int> Xb = new List<int>();

            for(int Y = 0; Y <= height; ++ Y)
            {
				Xb.Clear();
				Xb.AddRange(Get_Borders(polygon, Y));
                Xb.Sort();

                int borders = Xb.Count() - 1;
                for(int b = 0; b < borders; b += 2)
				{
                    len = Xb[b + 1] - Xb[b];
					if(len >= 2)
						g.DrawLine(obj_pen, Xb[b],		   Y,
											Xb[b + 1] - 1, Y); // (- 1) - почему то на 1-цу шире
					else if(len == 1)
						g.DrawImageUnscaled(pt, Xb[b], Y);

					if(!draw && len >= 1)	draw = true;
				}
            }

			obj_pen.Width = backup;

			return draw;
		}

		public static void Paint_Single_Frame(Graphics g, Pen select_pen, int height, List<PointD> polygon, bool solid)
		{
			int last_id = polygon.Count() - 1;

			for(int VC = 0, VN = 0; VC <= last_id; ++ VC)
			{
				if(VC < last_id)	VN = VC + 1;
				else				VN = 0;

                Line(g, select_pen, height, solid, polygon[VC],
												   polygon[VN], true);
			}
		}

        public static void Paint_Bulk_Frame(Graphics g, Pen select_pen, int height, List<List<PointD>> figure)
		{
			float backup = select_pen.Width;
			select_pen.Width = 1.0F;

			int len = 0;
			Bitmap pt = new Bitmap(1, 1);
			pt.SetPixel(0, 0, select_pen.Color);

			List<int> Xb = new List<int>();

			for(int Y = 0; Y <= height; ++ Y)
			{
				Xb.Clear();

				Xb.AddRange(Get_Borders(figure[0], Y));
				Xb.AddRange(Get_Borders(figure[1], Y));

				Xb.Sort();

				int borders = Xb.Count() - 1;
				for(int b = 0; b < borders; b += 2)
				{
                    len = Xb[b + 1] - Xb[b];
					if(len >= 2)
						g.DrawLine(select_pen, Xb[b],		  Y,
											   Xb[b + 1] - 1, Y); // (- 1) - почему то на 1-цу шире
					else if(len == 1)
						g.DrawImageUnscaled(pt, Xb[b], Y);
				}
			}

			select_pen.Width = backup;
        }

        public static List<int> Get_Borders(List<PointD> polygon, int Y)
		{
			double X = 0.0;

			List<int> Xb = new List<int>();
			int num_vertexs = polygon.Count() - 1;
			for(int C = 0, N; C <= num_vertexs; ++ C)
			{
				if(C < num_vertexs)		N = C + 1;
				else					N = 0;

				if((polygon[C].Y <  Y) && (polygon[N].Y >= Y)
				|| (polygon[C].Y >= Y) && (polygon[N].Y <  Y))
				{
					X = polygon[C].X + (Y - polygon[C].Y) * (polygon[N].X - polygon[C].X) /
															(polygon[N].Y - polygon[C].Y);
					Xb.Add((int)Double_Round(X));
				}
			}
			return Xb;
		}

		public static List<PointD> polygon_resize(List<PointD> polygon, double border, bool direction)
		{
			List<PointD> result_polygon = new List<PointD>();

			PointD R = new PointD();
			PointD A1 = new PointD();
			PointD B1 = new PointD();
			PointD A2 = new PointD();
			PointD B2 = new PointD();

			int last_id = polygon.Count() - 1;

			for(int C = 0, P, N; C <= last_id; ++ C)
			{
				if(C == 0)				P = last_id;
				else					P = C - 1;

				if(C < last_id)			N = C + 1;
				else					N = 0;

				A1.Copy(polygon[P]);
				A2.Copy(polygon[C]);
				B1.Copy(polygon[C]);
				B2.Copy(polygon[N]);

				offset(border, ref A1, ref A2, direction);
				offset(border, ref B1, ref B2, direction);

				STO.is_intersection(A1, A2, B1, B2, ref R);

				result_polygon.Add(new PointD(R));
			}

			return result_polygon;
		}

        public static bool offset(double BORDER, ref PointD A, ref PointD B, bool direction)
		{
            double dX = A.X - B.X;
            double dY = A.Y - B.Y;
            double hypotenuse = Math.Sqrt(dX * dX + dY * dY);
            if(hypotenuse != 0.0)
            {
			    int coef = 1;
			    if(direction)	coef *= -1;
			    A.X += BORDER / hypotenuse * dY * coef;
			    A.Y -= BORDER / hypotenuse * dX * coef;
			    B.X += BORDER / hypotenuse * dY * coef;
			    B.Y -= BORDER / hypotenuse * dX * coef;

                return true;
            }

            return false;
		}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public static void Circle(Graphics g, Pen obj_pen, int screen_height, bool solid, int X, int Y, double radius) {
			Circle(g, obj_pen, screen_height, solid, new Point(X, Y), radius);
		}

        public static void Circle(Graphics g, Pen obj_pen, int screen_height, bool solid, Point center, double radius)
		{
			if(!solid)
				if((int)Double_Round(obj_pen.Width) <= 1)
                    Paint_Single_Frame(g, obj_pen, screen_height, Get_Circle(center, (int)Double_Round(radius)), solid);
				else
				{
					List<List<PointD>> figure = new List<List<PointD>>();
					figure.Add(Get_Circle(center, (int)Double_Round(radius + obj_pen.Width / 2.0)));
					figure.Add(Get_Circle(center, (int)Double_Round(radius - obj_pen.Width / 2.0)));
					Paint_Bulk_Frame(g, obj_pen, screen_height, figure);
				}
			else
				if(!Paint_Polygon(g, obj_pen, screen_height, Get_Circle(center, (int)Double_Round(radius))))
				{
					Bitmap pt = new Bitmap(1, 1);
					pt.SetPixel(0, 0, obj_pen.Color);
					g.DrawImageUnscaled(pt, center);
				}
		}

        public static List<PointD> Get_Circle(int X, int Y, int radius) {
			return Get_Circle(new Point(X, Y), radius);
		}

        public static List<PointD> Get_Circle(Point center, int radius)
        {
			List<PointD> polygon = new List<PointD>();
			Point P = new Point(center.X, center.Y + radius);

			do
			{
                if(Math.Abs(STO.get_distance(new Point(P.X - 1, P.Y), center) - radius) < Math.Abs(STO.get_distance(new Point(P.X, P.Y - 1), center) - radius))
					P.X -= 1;
				else
					P.Y -= 1;

                polygon.Add(new PointD(P));
			}
            while(P.Y > center.Y);

			do
			{
				if(Math.Abs(STO.get_distance(new Point(P.X + 1, P.Y), center) - radius) < Math.Abs(STO.get_distance(new Point(P.X, P.Y - 1), center) - radius))
					P.X += 1;
				else
					P.Y -= 1;

                polygon.Add(new PointD(P));
			}
			while(P.X < center.X);

			do
			{
				if(Math.Abs(STO.get_distance(new Point(P.X + 1, P.Y), center) - radius) < Math.Abs(STO.get_distance(new Point(P.X, P.Y + 1), center) - radius))
					P.X += 1;
				else
					P.Y += 1;

                polygon.Add(new PointD(P));
			}
			while(P.Y < center.Y);

			do
			{
				if(Math.Abs(STO.get_distance(new Point(P.X - 1, P.Y), center) - radius) < Math.Abs(STO.get_distance(new Point(P.X, P.Y + 1), center) - radius))
					P.X -= 1;
				else
					P.Y += 1;

                polygon.Add(new PointD(P));
			}
            while(P.X > center.X);

			return Optimization_Polygon(ref polygon);
		}

		public static List<PointD> Optimization_Polygon(ref List<PointD> polygon)
		{
            int num_vertexs = polygon.Count();
            if(num_vertexs >= 3)
            {
				int last_id = num_vertexs - 1;

			    for(int C = last_id, P, N; C >= 0; -- C)
			    {
				    if(C == 0)				N = last_id;
				    else					N = C - 1;

				    if(C < last_id)			P = C + 1;
				    else					P = 0;

					if(((polygon[P].X - polygon[N].X) * (polygon[C].Y - polygon[N].Y) -
						(polygon[C].X - polygon[N].X) * (polygon[P].Y - polygon[N].Y)) / 2.0 == 0.0)
                    {
                        polygon.RemoveAt(C);

						if(-- last_id < 2)		break;
                    }
			    }
            }

			return polygon;
		}
    }
}
