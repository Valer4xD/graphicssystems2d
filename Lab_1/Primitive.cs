﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading;
using System.Drawing;

using System.Runtime.Serialization;


namespace Lab_1 {
    [Serializable]
    public class Primitive
	{
		public List<PointD> skelet = new List<PointD>();
		public List<List<PointD>> template = new List<List<PointD>>();
		public PointD center_template = new PointD();

		public int type = 0;

        public List<List<PointD>> multi_figure = new List<List<PointD>>();

		public List<Animation> queue_animations = new List<Animation>(),
								 run_animations = new List<Animation>();
        [NonSerialized]
        private Timer timer;
        public Color color = Color.Black;
        public float width = 1.0F;
        [NonSerialized]
		public Pen pen = new Pen(Color.Black, 1);

		public PointD Center = new PointD();

		const int MOVE = 1,
				  ZOOM = 2,
				  TURN = 3;

		public virtual void update_creating(double X, double Y) {}
		public virtual bool end_creating() { return false; }
		public virtual void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test) {}
		public virtual void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height) {}
		public virtual bool Is_Include(double X, double Y) { return false; }
		public virtual void Convert() {}

        [OnDeserialized]
        private void OnDeserializedMethod(StreamingContext context)
        {
            pen.Color = this.color;
			pen.Width = this.width;
        }

		protected bool Is_Include_Poligon(List<PointD> polygon, double X, double Y)
		{
            int intersections = 0;
			int num_vertexs = polygon.Count() - 1;
			for(int C = 0, N; C <= num_vertexs; ++ C)
			{
				if(C < num_vertexs)		N = C + 1;
				else					N = 0;

				if((polygon[C].Y <  Y && polygon[N].Y >= Y
				||  polygon[C].Y >= Y && polygon[N].Y <  Y)
				&& X >= polygon[C].X +
					   (Y - polygon[C].Y)		 *
					   (polygon[N].X - polygon[C].X) /
					   (polygon[N].Y - polygon[C].Y))           ++ intersections;
			}
			if(intersections % 2 != 0)		return true;
			return false;
		}

		public virtual void Push(double X, double Y) {
			multi_figure.Add(new List<PointD>() { new PointD(X, Y) });
		}

		public virtual void Push(Point P) {
			multi_figure.Add(new List<PointD>() { new PointD(P) });
		}
		public virtual void Push(PointD P) {
			multi_figure.Add(new List<PointD>() { P });
		}

        public virtual void Push(List<Point> polygon) {
			Push(Polygon_Int_to_Double(polygon));
		}

        public virtual void Push(List<PointD> polygon) {
			Push_Polygon(ref multi_figure, polygon);
		}

		public static List<PointD> Polygon_Int_to_Double(List<Point> polygon_int)
		{
			List<PointD> polygon_double = new List<PointD>();
			int num_vertexs = polygon_int.Count();
			for(int V = 0; V < num_vertexs; ++ V)
				polygon_double.Add(new PointD(polygon_int[V]));
			return polygon_double;
		}

		public static void Push_Polygon(ref List<List<PointD>> figure, List<Point> polygon) {
			Push_Polygon(ref figure, Polygon_Int_to_Double(polygon));
		}

		public static void Push_Polygon(ref List<List<PointD>> figure, List<PointD> polygon)
		{
			figure.Add(new List<PointD>());
			int last_polygon = figure.Count() - 1;
			int num_vertexs = polygon.Count();
			for(int V = 0; V < num_vertexs; ++ V)
				figure[last_polygon].Add(new PointD(polygon[V]));
		}
/*
		public static void Copy_Polygon(ref List<PointD> in_polygon, List<Point> out_polygon)
		{
			int num_vertexs = out_polygon.Count();
			for(int V = 0; V < num_vertexs; ++ V)
				in_polygon.Add(new PointD(out_polygon[V]));
		}

		public static void Copy_Polygon(ref List<PointD> in_polygon, List<PointD> out_polygon)
		{
			int num_vertexs = out_polygon.Count();
			for(int V = 0; V < num_vertexs; ++ V)
				in_polygon.Add(new PointD(out_polygon[V]));
		}
*/
		protected void Paint_Polygon(Graphics g, Pen obj_pen, int height, List<PointD> polygon)
		{
			List<int> Xb = new List<int>();

            for(int Y = 0; Y <= height; ++ Y)
            {
				Xb.Clear();

				Xb.AddRange(Get_Borders(polygon, Y));

                Xb.Sort();
                int borders = Xb.Count() - 1;
                for(int b = 0; b < borders; b += 2)
                    g.DrawLine(obj_pen, Xb[b],     Y,
                                        Xb[b + 1] - 1, Y);
            }
		}

		protected List<int> Get_Borders(List<PointD> polygon, int Y)
		{
			List<int> Xb = new List<int>();
			int num_vertexs = polygon.Count() - 1;
			for(int C = 0, N; C <= num_vertexs; ++ C)
			{
				if(C < num_vertexs)		N = C + 1;
				else					N = 0;

				if((polygon[C].Y <  Y) && (polygon[N].Y >= Y)
				|| (polygon[C].Y >= Y) && (polygon[N].Y <  Y))
					Xb.Add((int)Math.Round((polygon[C].X +
								           (Y - polygon[C].Y) *
										   (polygon[N].X - polygon[C].X) /
										   (polygon[N].Y - polygon[C].Y)),
											MidpointRounding.AwayFromZero));
			}
			return Xb;
		}

		protected void Paint_Line_Round(Graphics g, Pen obj_pen, PointD A, PointD B)
		{
			g.DrawLine(obj_pen, (int)Math.Round(A.X, MidpointRounding.AwayFromZero),
                                (int)Math.Round(A.Y, MidpointRounding.AwayFromZero),
                                (int)Math.Round(B.X, MidpointRounding.AwayFromZero),
                                (int)Math.Round(B.Y, MidpointRounding.AwayFromZero));
        }

		protected void Paint_Line_Round(Graphics g, Pen obj_pen, double AX, double AY, double BX, double BY)
		{
			g.DrawLine(obj_pen, (int)Math.Round(AX, MidpointRounding.AwayFromZero),
                                (int)Math.Round(AY, MidpointRounding.AwayFromZero),
                                (int)Math.Round(BX, MidpointRounding.AwayFromZero),
                                (int)Math.Round(BY, MidpointRounding.AwayFromZero));
        }

        public bool offset(double BORDER, ref PointD A, ref PointD B, bool direction)
		{
            double dX = A.X - B.X;
            double dY = A.Y - B.Y;
            double hypotenuse = Math.Sqrt(dX * dX + dY * dY);
            if(hypotenuse != 0.0)
            {
			    int coef = 1;
			    if(direction)	coef *= -1;
			    A.X += BORDER / hypotenuse * dY * coef;
			    A.Y -= BORDER / hypotenuse * dX * coef;
			    B.X += BORDER / hypotenuse * dY * coef;
			    B.Y -= BORDER / hypotenuse * dX * coef;
                return true;
            }
            return false;
		}

		public void Reverse()
		{
			int num_polygons = multi_figure.Count();
			for(int P = 0; P < num_polygons; ++ P)		multi_figure[P].Reverse();
		}

// Геометрические преобразования ---------------------------------------------------------

		public virtual PointD Get_Center()
        {
			PointD center = new PointD();
			List<PointD> polygon = new List<PointD>();
			List<PointD> Z = multi_figure[0];
			double Xmin = Z[0].X,
				   Xmax = Z[0].X,
				   Ymin = Z[0].Y,
				   Ymax = Z[0].Y;
			int num_polygons = multi_figure.Count();
			for(int P = 0; P < num_polygons; ++ P)
			{
				polygon = multi_figure[P];
                int num_vertexs = polygon.Count() - 1;
				for(int Vx = 0; Vx <= num_vertexs; ++ Vx)
				{
					if(polygon[Vx].X < Xmin)   Xmin = polygon[Vx].X;
					if(polygon[Vx].X > Xmax)   Xmax = polygon[Vx].X;
					if(polygon[Vx].Y < Ymin)   Ymin = polygon[Vx].Y;
					if(polygon[Vx].Y > Ymax)   Ymax = polygon[Vx].Y;
				}
			}
            center.X = (Xmax - Xmin) / 2 + Xmin;
            center.Y = (Ymax - Ymin) / 2 + Ymin;
			return center;
        }

		public void add_animation(Animation animation) {
			queue_animations.Add(animation);
		}

		public void start_animation()
		{
            lock(Form1.locker)
			    if(run_animations.Count() > 0 && timer == null)
			    {
				    PointD new_center = Get_Center();
				    run_animations[0].axis_X_zoom += new_center.X - Center.X;
				    run_animations[0].axis_Y_zoom += new_center.Y - Center.Y;
				    run_animations[0].axis_X_turn += new_center.X - Center.X;
				    run_animations[0].axis_Y_turn += new_center.Y - Center.Y;
				    Center.X = new_center.X;
				    Center.Y = new_center.Y;

				    TimerCallback timer_func = new TimerCallback(animation_tick);
				    timer = new Timer(animation_tick, this, 0, run_animations[0].time_out);
			    }
		}

        private static void animation_tick(object state)
        {
            lock(Form1.locker)
            {
			    Primitive primitive = (Primitive)state;

                if(primitive.run_animations.Count() > 0)
                    if(-- primitive.run_animations[0].iterations == 0)
					{
						primitive.run_animations.RemoveAt(0);
						primitive.timer.Dispose();
						primitive.timer = null;
						primitive.start_animation();
					}
					else
					{
						int num_polygons = primitive.multi_figure.Count();
						for(int P = 0; P < num_polygons; ++ P)
							primitive.Polygon_Transformation(primitive.multi_figure[P], primitive.run_animations[0]);

                        primitive.Polygon_Transformation(primitive.skelet, primitive.run_animations[0]);
					}
				else
				{
					primitive.timer.Dispose();
					primitive.timer = null;
				}
            }
        }

		public void Polygon_Transformation(List<PointD> polygon, Animation animation)
		{
			List<List<double>> multi_matrix = new List<List<double>>();
			int num_completed	= 0,
				need_offset		= 0,
				num_need_offset	= 0,
				i				= 0;

			bool single_center = animation.Single_Center();

			if(animation.zoom_priority >= 1 && animation.zoom_priority <= 3)	++ num_need_offset;
			if(animation.turn_priority >= 1 && animation.zoom_priority <= 3)	++ num_need_offset;

			int num_vertexs = polygon.Count();
            for(int Vx = 0; Vx < num_vertexs; ++ Vx)
            {
				multi_matrix.Clear();
				multi_matrix.Add(new List<double>() { 1, 0, 0 });
				multi_matrix.Add(new List<double>() { 0, 1, 0 });
				multi_matrix.Add(new List<double>() { 0, 0, 1 });

                num_completed = 0;
				need_offset	  = 0;

				for(i = 1; i <= 3; ++ i)
                {
                    
                    if(animation.move_priority == i) {
                        if(++ num_completed > 3)   break;
						multi_matrix = add_matrix(animation, MOVE, multi_matrix, true, true);
                    }
                    if(animation.zoom_priority == i) {
                        if(++ num_completed > 3)   break;
						++ need_offset;
						multi_matrix = add_matrix(animation, ZOOM, multi_matrix,
												  (need_offset == 1               || !single_center) ? true : false,
												  (need_offset == num_need_offset || !single_center) ? true : false);
                    }
                    if(animation.turn_priority == i) {
                        if(++ num_completed > 3)   break;
						++ need_offset;
						multi_matrix = add_matrix(animation, TURN, multi_matrix,
												  (need_offset == 1               || !single_center) ? true : false,
												  (need_offset == num_need_offset || !single_center) ? true : false);
                   }
                }
				if(num_completed > 0)
				{
					List<List<double>> cur_matrix = new List<List<double>>();
					cur_matrix.Add(new List<double>() { polygon[Vx].X, polygon[Vx].Y, 1 });
					Matrix.multiplication(ref cur_matrix, multi_matrix);
					polygon[Vx] = new PointD(cur_matrix[0][0], cur_matrix[0][1]);
				}
            }
		}

		private List<List<double>> get_move_matrix(double move_delta_X, double move_delta_Y)
		{
			List<List<double>> move_matrix = new List<List<double>>();
			move_matrix.Add(new List<double>() { 1,            0,            0 });
			move_matrix.Add(new List<double>() { 0,            1,            0 });
			move_matrix.Add(new List<double>() { move_delta_X, move_delta_Y, 1 });

			return move_matrix;
		}

        private List<List<double>> add_matrix(Animation animation, int reform_type, List<List<double>> multi_matrix, bool move_zero, bool move_back)
		{
			switch(reform_type)
            {
                case MOVE:
                {
					Matrix.multiplication(ref multi_matrix,
										  get_move_matrix(	   animation.move_delta_X,
															   animation.move_delta_Y));
                    break; }
                case ZOOM:
                {
					List<List<double>> zoom_matrix = new List<List<double>>();
					zoom_matrix.Add(new List<double>() { animation.zoom_delta_X, 0,					  0 });
					zoom_matrix.Add(new List<double>() { 0,					  animation.zoom_delta_Y, 0 });
					zoom_matrix.Add(new List<double>() { 0,					  0,					  1 });

					if(move_zero)
						Matrix.multiplication(ref multi_matrix,
											  get_move_matrix(-1 * animation.axis_X_zoom,
															  -1 * animation.axis_Y_zoom));
					Matrix.multiplication(ref multi_matrix,
											  zoom_matrix);
					if(move_back)
						Matrix.multiplication(ref multi_matrix,
											  get_move_matrix(	   animation.axis_X_zoom,
																   animation.axis_Y_zoom));
                    break; }
                case TURN:
                {
					double radians = animation.turn_delta * Math.PI / 180.0;

					List<List<double>> turn_matrix = new List<List<double>>();
					turn_matrix.Add(new List<double>() {	  Math.Cos(radians), Math.Sin(radians), 0 });
					turn_matrix.Add(new List<double>() { -1 * Math.Sin(radians), Math.Cos(radians), 0 });
					turn_matrix.Add(new List<double>() {	  0,				 0,					1 });

					if(move_zero)
						Matrix.multiplication(ref multi_matrix,
											  get_move_matrix(-1 * animation.axis_X_turn,
															  -1 * animation.axis_Y_turn));
					Matrix.multiplication(ref multi_matrix,
											  turn_matrix);
					if(move_back)
						Matrix.multiplication(ref multi_matrix,
											  get_move_matrix(	   animation.axis_X_turn,
																   animation.axis_Y_turn));
                    break; }
            }

            return multi_matrix;
        }
    }
}