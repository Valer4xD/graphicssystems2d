﻿using System;

using System.Drawing;

using System.Linq;

namespace Lab_1 {
    [Serializable]
    public class Line_Blank : Primitive
	{
		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{
			for(int C = 0, N = 0; C < 4; ++ C)
			{
				if(C < 3)	N = C + 1;
				else		N = 0;
				Paint_Line_Round(g, select_pen, multi_figure[1][C], multi_figure[1][N]);
			}
        }

		protected void Paint_Points(Graphics g, Pen draw_pen, int last)
		{
			g.DrawEllipse(draw_pen, (int)Math.Round(multi_figure[0][   0].X - 2, MidpointRounding.AwayFromZero),
									(int)Math.Round(multi_figure[0][   0].Y - 2, MidpointRounding.AwayFromZero), 5, 5);
			g.DrawEllipse(draw_pen, (int)Math.Round(multi_figure[0][last].X - 2, MidpointRounding.AwayFromZero),
									(int)Math.Round(multi_figure[0][last].Y - 2, MidpointRounding.AwayFromZero), 5, 5);
		}

		public override bool Is_Include(double X, double Y)
		{
			if(multi_figure.Count() > 1)
				if(Is_Include_Poligon(multi_figure[1], X, Y))	return true;
			else
				if(Is_Include_Poligon(multi_figure[0], X, Y))	return true;
			return false;
		}
	}
}