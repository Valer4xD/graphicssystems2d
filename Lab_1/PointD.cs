﻿using System;

using System.Drawing;

namespace Lab_1 {
    [Serializable]
    public class PointD
    {
		public double X = 0,
			          Y = 0;

		public PointD() {}

		public PointD(double x, double y)
		{
			X = x;
			Y = y;
		}

		public PointD(int x, int y)
		{
			X = x;
			Y = y;
		}

		public PointD(PointD V)
		{
			X = V.X;
			Y = V.Y;
		}

		public PointD(Point V)
		{
			X = V.X;
			Y = V.Y;
		}

		public PointD Copy(double x, double y)
		{
			X = x;
			Y = y;
            return this;
		}

		public PointD Copy(int x, int y)
		{
			X = x;
			Y = y;
            return this;
		}

		public PointD Copy(PointD V)
		{
			X = V.X;
			Y = V.Y;
            return this;
		}

		public PointD Copy(Point V)
		{
			X = V.X;
			Y = V.Y;
            return this;
		}
	}
}