﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

using System.IO;

namespace Lab_1
{
    [Serializable]
    public class Circle : Primitive
    {
		public PointD first_point  = new PointD(0.0, 0.0);
		public PointD second_point = new PointD(0.0, 0.0);

        public void Set_Vertexs()
		{
			if(skelet.Count() > 0)		skelet.RemoveAt(0);
			if(skelet.Count() > 0)		skelet.RemoveAt(0);
            skelet.InsertRange(0, new List<PointD>() { first_point, second_point });
			const double EPS = 0.02; // Макс. погр. ICC = 0.01
            if(Get_Radius() < EPS)
			{
                skelet[1].X = skelet[0].X;
                skelet[1].Y = skelet[0].Y + 0.5;
			}
        }

        public void start_creating(double X, double Y)
        {
			first_point  = new PointD(X, Y);
            second_point = new PointD(X, Y);
			Set_Vertexs();
		}

		public override void update_creating(double X, double Y)
		{
			second_point = new PointD(X, Y);
            Set_Vertexs();
		}

        public override bool end_creating()
        {
            return true;
        }

        public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
			// MyDraw.Circle(g, draw_pen, screen_height, true, skelet);

            List<PointD> polygon = MyDraw.Get_Circle(MyDraw.PointD_to_Point(Get_Center()),
												   (int)MyDraw.Double_Round(Get_Radius()));
			polygon = MyDraw.polygon_resize(polygon, 0.5, true);
            MyDraw.Paint_Polygon(g, draw_pen, screen_height, polygon);

			// MyDraw.Circle(g, draw_pen, screen_height, true, Get_Center(), Get_Radius());
		}

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
		{
			if((int)Math.Round(select_pen.Width, MidpointRounding.AwayFromZero) <= 1)
			{
				List<PointD> polygon = MyDraw.Get_Circle(MyDraw.PointD_to_Point(Get_Center()),
													   (int)MyDraw.Double_Round(Get_Radius()));
/*
				string fileName = "out.txt";
				FileStream aFile = new FileStream(fileName, FileMode.OpenOrCreate);
				StreamWriter sw = new StreamWriter(aFile);
				aFile.Seek(0, SeekOrigin.End);
				int num_vertexs = polygon.Count();
				for(int i = 0; i < num_vertexs; ++ i)
					sw.WriteLine("{0} - X = {1}, Y = {2}", i, polygon[i].X, polygon[i].Y);
				sw.WriteLine("=========================================================");
				sw.Close();
*/
				polygon = MyDraw.polygon_resize(polygon, 1.0, true);

				MyDraw.Paint_Single_Frame(g, select_pen, screen_height, polygon, true);
			}
			else
			{
				double radius_circle = Get_Radius();

				if(radius_circle < 0.5)
					radius_circle = 0.5;

				double radius_borders = radius_circle + select_pen.Width / 2.0;

                MyDraw.Circle(g, select_pen, screen_height, false, MyDraw.PointD_to_Point(Get_Center()), radius_borders);
			}
		}

		public double Get_Radius()
		{
			return STO.get_distance(skelet[0], skelet[1]);
		}

		public override bool Is_Include(double X, double Y)
		{
			if(STO.get_distance(skelet[0], new PointD(X, Y)) <= STO.get_distance(skelet[0], skelet[1])
			|| Math.Abs(skelet[0].X - X) <= 0.5 && Math.Abs(skelet[0].Y - Y) <= 0.5)
				return true;
			return false;
		}

		public override PointD Get_Center()
		{
			return skelet[0];
		}
    }
}