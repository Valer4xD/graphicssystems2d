﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace Lab_1
{
    [Serializable]
    public class Multi_Figure : Primitive
    {
        List<List<Broken_Line>> _clock_wise_broken_lines = new List<List<Broken_Line>>();
        List<List<Broken_Line>> _counter_cw_broken_lines = new List<List<Broken_Line>>();

        List<Broken_Line> _broken_lines = new List<Broken_Line>();
        List<PointD> _polygon_with_intersections = new List<PointD>();

        List<bool> _done_broken_lines = new List<bool>();

        int _clock_wise = 0;
        int _counter_cw = 0;

        List<int> _clock_wise_ids = new List<int>();
        List<int> _counter_cw_ids = new List<int>();
        List<List<PointD>> _clock_wise_xy = new List<List<PointD>>();
        List<List<PointD>> _counter_cw_xy = new List<List<PointD>>();

        public void Push_Check(List<Point> polygon) => Push_Check(Polygon_Int_to_Double(polygon));
        public void Push_Check(List<PointD> polygon)
        {
            _broken_lines.Clear();
            _polygon_with_intersections.Clear();

            int V_PWIS = 0;

            _broken_lines.Add(new Broken_Line());
            int last_BL = 0;

            int num_vertexs = polygon.Count() - 1,
                VC  = 0,
                VS  = 0,
                VCN = 0,
                VSN = 0,
                VSW = 0;

            List<int> VSP = new List<int>();

            bool found   = false,
                 reverse = false;

            PointD candidate_point    = new PointD(),
                   intersection_point = new PointD(),
                   cur_point          = new PointD();

            double min_dist = 0,
                   new_dist = 0;

            int num_polygons,
                P;

            for(VC = 0; VC <= num_vertexs; ++ VC)
            {
                _polygon_with_intersections.Add(new PointD(polygon[VC]));

                _broken_lines[last_BL].Push(polygon[VC]);
                _broken_lines[last_BL].vertexs_id.Add(V_PWIS ++);

                cur_point = new PointD(polygon[VC].X, polygon[VC].Y);

                if(VC < num_vertexs)    VCN = VC + 1;
                else                    VCN = 0;

                VSP.Clear();
                do
                {
                    found    = false;
                    min_dist = 0;

                    for(VS = 0; VS <= num_vertexs; ++ VS)
                    {
                        if(VS < num_vertexs)    VSN = VS + 1;
                        else                    VSN = 0;

                        if(VSP.Contains(VS))            continue; // Найденные пересекающие отрезки. 
                        if(VSN == VC || VCN == VS)      continue; // Соседние отрезки.
                        if(VC == VS)                    continue; // Тот же отрезок.

                        if(STO.is_intersection(cur_point  , polygon[VCN],
                                               polygon[VS], polygon[VSN], ref candidate_point))
                        {
                            new_dist = STO.get_distance(cur_point, candidate_point);

                            if(Math.Abs(new_dist - min_dist) < 0.000001)
                                throw new InvalidOperationException("Несколько пересечений в одной точке!");

                            if(new_dist < min_dist || !found)
                            {
                                found = true;
                                min_dist = new_dist;

                                VSW = VS;
                                intersection_point = new PointD(candidate_point.X, candidate_point.Y);
                            }
                        }
                    }

                    if(found)
                    {
                        VSP.Add(VSW);

                        cur_point = new PointD(intersection_point.X, intersection_point.Y);

                        if(!reverse)
                        {
                            reverse = true;

                            _broken_lines[last_BL].Set_Out(0, 0, 0, 0, VC, VSW);

                            ++ last_BL;
                            _broken_lines.Add(new Broken_Line());
                            _broken_lines[last_BL].Set_Out(0, 0, 0, 0, VC, VSW);
                        }
                        else
                        {
                            _polygon_with_intersections.Add(new PointD(intersection_point));
                            _broken_lines[last_BL].vertexs_id.Add(V_PWIS);

                            reverse = false;

                            _broken_lines[last_BL].Push(intersection_point);
                            _broken_lines[last_BL].Set_In(0, 0, 0, 0, VC, VSW);

                            if(_broken_lines[last_BL].broken_line.Count() > 1)
                                _broken_lines[last_BL].broken_line.Reverse();

                            ++ last_BL;
                            _broken_lines.Add(new Broken_Line());
                            _broken_lines[last_BL].Push(intersection_point);
                            _broken_lines[last_BL].Set_In(0, 0, 0, 0, VC, VSW);

                            _broken_lines[last_BL].vertexs_id.Add(V_PWIS);
                            ++ V_PWIS;
                        }
                    }
                }
                while(found);
            }

            if(last_BL > 0)
            {
                _broken_lines[0].in_FC = _broken_lines[last_BL].in_FC;
                _broken_lines[0].in_FS = _broken_lines[last_BL].in_FS;
                _broken_lines[0].in_PC = _broken_lines[last_BL].in_PC;
                _broken_lines[0].in_PS = _broken_lines[last_BL].in_PS;
                _broken_lines[0].in_VC = _broken_lines[last_BL].in_VC;
                _broken_lines[0].in_VS = _broken_lines[last_BL].in_VS;
                _broken_lines[0].broken_line.InsertRange(0, _broken_lines[last_BL].broken_line);
                _broken_lines[0].vertexs_id.InsertRange(0, _broken_lines[last_BL].vertexs_id);
                _broken_lines.RemoveAt(last_BL);

                multi_figure.AddRange(collector(true));

                num_polygons = multi_figure.Count();
                for(P = 0; P < num_polygons; ++ P)
                    if(!STO.clockwise_direction(multi_figure[P]))
                        multi_figure[P].Reverse();
            }
            else
            {
                multi_figure.Add(new List<PointD>());
                int last_poligon = multi_figure.Count() - 1;
                for(int V = 0; (V <= num_vertexs); ++ V)
                {
                    PointD vertex = new PointD(polygon[V].X, polygon[V].Y);
                    multi_figure[last_poligon].Add(vertex);
                }
                if(!STO.clockwise_direction(multi_figure[last_poligon]))
                    multi_figure[last_poligon].Reverse();
            }
        }

        private List<List<PointD>> collector(bool union)
        {
            _clock_wise_broken_lines.Clear();
            _counter_cw_broken_lines.Clear();

            List<List<Broken_Line>> polygons_in_broken_lines = new List<List<Broken_Line>>();
            List<List<PointD>> anti_polygons = new List<List<PointD>>(),
                               union_polygon = new List<List<PointD>>();
            List<List<bool>> done_broken_lines_PIBL = new List<List<bool>>();
            bool clock_wise = false,
                 start      = false;
            int   num_broken_lines_PIBL =  0,
                count_broken_lines_PIBL =  0,
                num_polygons            =  0,
                P                       =  0;
            Broken_Line start_broken_line = new Broken_Line();
            Broken_Line  last_broken_line = new Broken_Line();

            _done_broken_lines.Clear();

            _clock_wise_ids.Clear();
            _counter_cw_ids.Clear();
            _clock_wise_xy.Clear();
            _counter_cw_xy.Clear();

            int num_done = 0;

            int BL = 0,
                num_broken_lines = _broken_lines.Count();

            for(BL = 0; BL < num_broken_lines; ++ BL)
                if(_broken_lines[BL].out_VS == _broken_lines[BL].in_VC
                && _broken_lines[BL].out_VC == _broken_lines[BL].in_VS)
                {
                    num_done ++;
                    _done_broken_lines.Add(true);
                    if(STO.clockwise_direction(_broken_lines[BL].broken_line))
                    {
                        _clock_wise_ids.Add(BL);
                        Push_Polygon(ref _clock_wise_xy, _broken_lines[BL].broken_line);
                        _clock_wise_broken_lines.Add(new List<Broken_Line>() { _broken_lines[BL] });
                    }
                    else
                    {
                        _counter_cw_ids.Add(BL);
                        Push_Polygon(ref _counter_cw_xy, _broken_lines[BL].broken_line);
                        _counter_cw_broken_lines.Add(new List<Broken_Line>() { _broken_lines[BL] });
                    }
                }
                else
                    _done_broken_lines.Add(false);

            Thread thread_clock_wise = new Thread(new ParameterizedThreadStart(thread_func));
            Thread thread_counter_cw = new Thread(new ParameterizedThreadStart(thread_func));
            thread_clock_wise.Priority = ThreadPriority.BelowNormal;
            thread_counter_cw.Priority = ThreadPriority.BelowNormal;
            thread_clock_wise.Start(true);
            thread_counter_cw.Start(false);

            int tics = 0;
            int pause_ms = 15; // От 0 до 15 разницы нет, будет 15.
            bool run = true;
            while(run)
            {
                Thread.Sleep(pause_ms);

                run = true;

                if(_clock_wise + num_done >= num_broken_lines
                || _counter_cw + num_done >= num_broken_lines)      run = false; // Есть сомнения.

                tics += pause_ms;
                if(tics >= 60000)
                    throw new InvalidOperationException("Превышен интервал ожидания!");
            }

            thread_clock_wise.Abort();
            thread_counter_cw.Abort();

            if(!union)
                if(_clock_wise_ids.Count() >= _counter_cw_ids.Count()) // Есть сомнения.
                    return _clock_wise_xy;
                else
                    return _counter_cw_xy;

            if(_clock_wise_ids.Count() >= _counter_cw_ids.Count()) // Есть сомнения.
            {
                clock_wise = true;
                polygons_in_broken_lines = _clock_wise_broken_lines;
            }
            else
                polygons_in_broken_lines = _counter_cw_broken_lines;

            num_polygons = polygons_in_broken_lines.Count();
            for(P = 0; P < num_polygons; ++ P)
            {
                num_broken_lines = polygons_in_broken_lines[P].Count();
                num_broken_lines_PIBL += num_broken_lines;
                done_broken_lines_PIBL.Add(new List<bool>());
                for(BL = 0; BL < num_broken_lines; ++ BL)
                    done_broken_lines_PIBL[P].Add(false);
            }

            bool progress = true;
            num_polygons = polygons_in_broken_lines.Count();
            while(count_broken_lines_PIBL < num_broken_lines_PIBL)
            {
                progress = false;
                for(P = 0; P < num_polygons; ++ P)
                {
                    num_broken_lines = polygons_in_broken_lines[P].Count();
                    for(BL = 0; BL < num_broken_lines; ++ BL)
                        if(!done_broken_lines_PIBL[P][BL])
                            if(!start)
                            {
                                start = true;

                                done_broken_lines_PIBL[P][BL] = true;

                                progress = true;
                                ++ count_broken_lines_PIBL;

                                start_broken_line = last_broken_line = polygons_in_broken_lines[P][BL];
                                anti_polygons.Add(new List<PointD>());
                                anti_polygons[anti_polygons.Count() - 1].AddRange(last_broken_line.broken_line);

                                break;
                            }
                            else
                                if(last_broken_line.out_VS == polygons_in_broken_lines[P][BL].in_VC
                                && last_broken_line.out_VC == polygons_in_broken_lines[P][BL].in_VS)
                                {
                                    done_broken_lines_PIBL[P][BL] = true;

                                    progress = true;
                                    ++ count_broken_lines_PIBL;

                                    last_broken_line = polygons_in_broken_lines[P][BL];
                                    anti_polygons[anti_polygons.Count() - 1].AddRange(last_broken_line.broken_line);

                                    if(last_broken_line.out_VS == start_broken_line.in_VC
                                    && last_broken_line.out_VC == start_broken_line.in_VS)      start = false;

                                    break;
                                }
                }
                if( ! progress)
                    throw new InvalidOperationException("Бесконечный цикл!");
            }

            num_polygons = anti_polygons.Count();
            
            for(P = 0; P < num_polygons; ++ P)
                if(clock_wise == STO.clockwise_direction(anti_polygons[P]))
                {
                    union_polygon.Add(anti_polygons[P]);
                    break;
                }
            
            /*
            // Если цикл выше заменить этим соберёт многоугольники, которые не закрашиваются при union == false,
            // за исключением типа {800 200 - 800 600 - 200 600 - 200 200 - 600 400 - 400 400}.
            for(P = 0; P < num_polygons; ++ P)
                if(clock_wise != STO.clockwise_direction(anti_polygons[P]))
                    union_polygon.Add(anti_polygons[P]);
            */

            return union_polygon;
        }

        private void thread_func(object clock_wise_object)
        {
            bool clock_wise = (bool)clock_wise_object;

            int vertex = 0,
                num_vertexs = _polygon_with_intersections.Count();

            List<int> vertexs_id = new List<int>();
            int BL  = 0,
                BL2 = 0,
                num_broken_lines = _broken_lines.Count();

            List<List<int>> tree = new List<List<int>>();
            int branch       = 0,
                num_branches = 0,
                level        = 0,
                num_levels   = 0;

            bool found    = false,
                 progress = true,
                 end      = false,
                 error    = false;

            List<PointD> temp_polygon = new List<PointD>();

            for(BL = 0; BL < num_broken_lines; ++ BL)
                if(!_done_broken_lines[BL]
                &&(clock_wise && !_clock_wise_ids.Contains(BL)
                ||!clock_wise && !_counter_cw_ids.Contains(BL)))
                {
                    tree.Clear();
                    tree.Add(new List<int>());
                    tree[0].Add(BL);

                    found = false;
                    num_levels = 0;

                    while(!found && num_levels < num_broken_lines - 1)
                    {
                        num_branches = tree.Count();
                        for(branch = num_branches - 1; branch >= 0; -- branch) // Разветвляем дерево.
                        {
                            tree.Insert(branch, new List<int>());
                            for(level = 0; level <= num_levels; ++ level)
                                tree[branch].Add(tree[branch + 1][level]);
                        }
                        num_branches *= 2;

                        branch = 0;
                        end = false;
                        while(!end)
                        {
                            progress = false;
                            for(BL2 = 0; BL2 < num_broken_lines; ++ BL2)
                                if(_broken_lines[tree[branch][num_levels]].out_VC == _broken_lines[BL2].in_VS
                                && _broken_lines[tree[branch][num_levels]].out_VS == _broken_lines[BL2].in_VC)
                                {
                                    tree[branch].Add(BL2);

                                    progress = true;
                                    ++ branch;

                                    if(branch >= num_branches)
                                    {
                                        end = true;
                                        break;
                                    }
                                }
                            if( ! progress)
                                throw new InvalidOperationException("Бесконечный цикл!");
                        }

                        ++ num_levels;

                        for(branch = 0; !found && branch < num_branches; ++ branch)
                        {
                            if(tree[branch][num_levels] == tree[branch][0])
                            {
                                error = false;

                                for(level = 0; level < num_levels; ++ level) // Проверка на повторы.
                                    if(clock_wise && _clock_wise_ids.Contains(tree[branch][level])
                                    ||!clock_wise && _counter_cw_ids.Contains(tree[branch][level]))
                                    {
                                        error = true;
                                        break;
                                    }

                                if(!error)
                                {
                                    vertexs_id.Clear();
                                    temp_polygon.Clear();

                                    for(level = 0; level < num_levels; ++ level)
                                    {
                                        vertexs_id.AddRange(_broken_lines[tree[branch][level]].vertexs_id);
                                        temp_polygon.AddRange(_broken_lines[tree[branch][level]].broken_line);
                                    }

                                    for(vertex = 0; vertex < num_vertexs; ++ vertex)
                                        if(!vertexs_id.Contains(vertex))
                                            if(Is_Include_Poligon(temp_polygon, _polygon_with_intersections[vertex].X, _polygon_with_intersections[vertex].Y))
                                            {
                                                error = true;
                                                break;
                                            }

                                    if(!error)
                                    {
                                        found = true;

                                        if(clock_wise)
                                        {
                                            if(STO.clockwise_direction(temp_polygon))
                                            {
                                                Push_Polygon(ref _clock_wise_xy, temp_polygon);
                                                _clock_wise_broken_lines.Add(new List<Broken_Line>());

                                                for(level = 0; level < num_levels; ++ level)
                                                {
                                                    _clock_wise_ids.Add(tree[branch][level]);
                                                    _clock_wise_broken_lines[_clock_wise_broken_lines.Count() - 1].Add(_broken_lines[tree[branch][level]]);

                                                    _clock_wise ++;
                                                }
                                            }
                                        }
                                        else
                                            if(!STO.clockwise_direction(temp_polygon))
                                            {
                                                Push_Polygon(ref _counter_cw_xy, temp_polygon);
                                                _counter_cw_broken_lines.Add(new List<Broken_Line>());

                                                for(level = 0; level < num_levels; ++ level)
                                                {
                                                    _counter_cw_ids.Add(tree[branch][level]);
                                                    _counter_cw_broken_lines[_counter_cw_broken_lines.Count() - 1].Add(_broken_lines[tree[branch][level]]);

                                                    _counter_cw ++;
                                                }
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
        }

        public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
        {
            List<int> Xb = new List<int>();

            for(int Y = 0; Y <= screen_height; ++ Y)
            {
                Xb.Clear();

                int num_polygons = multi_figure.Count();
                for(int P = 0; P < num_polygons; ++ P)
                    Xb.AddRange(Get_Borders(multi_figure[P], Y));

                Xb.Sort();
                int borders = Xb.Count() - 1;
                for(int b = 0; b < borders; b += 2)
                    g.DrawLine(draw_pen, Xb[b    ]    , Y,
                                         Xb[b + 1] - 1, Y);
            }
        }

        public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
        {
            List<PointD> polygon = new List<PointD>();
            int num_polygons = 0,
                num_vertexs  = 0,
                P = 0,
                VC = 0,
                VN = 0;

            num_polygons = multi_figure.Count();
            for(P = 0; P < num_polygons; ++ P)
            {
                polygon = multi_figure[P];
                num_vertexs = polygon.Count() - 1;
                for(VC = 0; VC <= num_vertexs; ++ VC)
                {
                    if(VC < num_vertexs)    VN = VC + 1;
                    else                    VN = 0;
                    Paint_Line_Round(g, select_pen, polygon[VC], polygon[VN]);
                }
            }
        }

        public override bool Is_Include(double X, double Y)
        {
            int intersections = 0;
            int num_polygons = multi_figure.Count();
            for(int P = 0; P < num_polygons; ++ P)
                if(Is_Include_Poligon(multi_figure[P], X, Y))
                    ++ intersections;
            if( intersections % 2 != 0)     return true;
            return false;
        }
    }
}