﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

namespace Lab_1
{
	[Serializable]
    public class Crosshair : Primitive
    {
        const double BORDER = 10.0,
					 CORRECTION_ZOOM = 1000.0;
		double RADIUS_TEMPLATE = 100.0;

		public PointD first_point  = new PointD(0.0, 0.0);
		public PointD second_point = new PointD(0.0, 0.0);

		public Crosshair()
		{
			skelet.Add(new PointD(0.0, 0.0));
			Build_Template(RADIUS_TEMPLATE);
		}

		public Crosshair(PointD P)
		{
			skelet.Add(P);
			Build_Template(RADIUS_TEMPLATE);

			skelet.Add(new PointD(P.X + RADIUS_TEMPLATE, P.Y));

			double extend = RADIUS_TEMPLATE * CORRECTION_ZOOM - RADIUS_TEMPLATE;
			double dX = skelet[1].X - skelet[0].X,
				   dY = skelet[1].Y - skelet[0].Y,
				   hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			skelet[1].X += extend / hypotenuse * dX;
			skelet[1].Y += extend / hypotenuse * dY;
		}

		public void Build_Template(double radius)
		{
			RADIUS_TEMPLATE = radius;

			center_template = new PointD(skelet[0]);

            template.Clear();
			template.Add(new List<PointD>());

			template[0].Add(new PointD(center_template.X - radius, center_template.Y));
			template[0].Add(new PointD(center_template.X + radius, center_template.Y));
			template[0].Add(new PointD(center_template.X,		   center_template.Y - radius));
			template[0].Add(new PointD(center_template.X,		   center_template.Y + radius));
		}

        public void Set_Vertexs()
		{
			skelet.Clear();
			double radius = STO.get_distance(first_point, second_point);
            skelet.AddRange(new List<PointD>() { first_point, second_point });
			// /* Перекрестие не вращается при создании.
            // skelet.AddRange(new List<PointD>() { first_point, new PointD(first_point.X, first_point.Y + radius) });
			// */
			const double EPS = 0.02; // Макс. погр. ICC = 0.01
            if(radius < EPS)
			{
				// radius = 0.5;
                skelet[1].X = skelet[0].X;
                skelet[1].Y = skelet[0].Y + 0.5;
			}
			// Build_Template(radius);

			radius = STO.get_distance(skelet[0], skelet[1]);
			double extend = radius * CORRECTION_ZOOM - radius;
			double dX = skelet[1].X - skelet[0].X,
				   dY = skelet[1].Y - skelet[0].Y,
				   hypotenuse = Math.Sqrt(dX * dX + dY * dY);
			skelet[1].X += extend / hypotenuse * dX;
			skelet[1].Y += extend / hypotenuse * dY;
        }

        public void start_creating(double X, double Y)
        {
			first_point  = new PointD(X, Y);
            second_point = new PointD(X, Y);
			Set_Vertexs();
		}

		public override void update_creating(double X, double Y)
		{
			second_point = new PointD(X, Y);
            Set_Vertexs();
		}

        public override bool end_creating()
        {
            return true;
        }

		public override void Paint(Graphics g, Pen draw_pen, int screen_width, int screen_height, bool test)
		{
			if((int)Math.Round(draw_pen.Width, MidpointRounding.AwayFromZero) <= 1)
				if(Get_Radius() > 0.5)
				{
					List<PointD> polygon = Transformation(template[0]);

                    MyDraw.Line(g, draw_pen, screen_height, true, polygon[0],
																  polygon[1], true);

                    MyDraw.Line(g, draw_pen, screen_height, true, polygon[2],
																  polygon[3], true);
				}
				else
				{
					Bitmap pt = new Bitmap(1, 1);
					pt.SetPixel(0, 0, draw_pen.Color);
					PointD center = Get_Center(); 
					g.DrawImageUnscaled(pt, (int)Math.Round(center.X, MidpointRounding.AwayFromZero),
											(int)Math.Round(center.Y, MidpointRounding.AwayFromZero));
				}
			else
			{
				double long_section = Get_Visual_Radius() - draw_pen.Width / 2.0,
					   width_beam	= draw_pen.Width;
                MyDraw.Paint_Polygon(g, draw_pen, screen_height, Get_Bulk_Crosshair(long_section, width_beam));
			}
		}

		public double Get_Radius()
		{
			if(skelet.Count() > 1)		return STO.get_distance(skelet[0], skelet[1]) / CORRECTION_ZOOM;
			else						return 0.5;
		}

		public double Get_Visual_Radius()
		{
			double radius = Get_Radius(),
				   visual_half_pen_width = pen.Width < 1.0 ? 0.5 : pen.Width / 2.0;
			if(radius < visual_half_pen_width)
				radius = visual_half_pen_width;
            else if(radius > 1.0)
                radius += 0.5;

            return radius;
		}

		public List<PointD> Transformation(List<PointD> polygon)
		{
			Animation animation = new Animation();

			animation.zoom_priority = 1;
			animation.zoom_delta_X = animation.zoom_delta_Y = Get_Radius() / RADIUS_TEMPLATE;

			animation.turn_priority = 2;
			animation.turn_delta = Get_Angle(); // Угол поворота в градусах

			animation.axis_X_turn = animation.axis_X_zoom = center_template.X;
			animation.axis_Y_turn = animation.axis_Y_zoom = center_template.Y;

			animation.move_priority = 3;
			animation.move_delta_X = skelet[0].X - center_template.X;
			animation.move_delta_Y = skelet[0].Y - center_template.Y;

			List<PointD> result_polygon = Copy_Polygon(polygon);
			Polygon_Transformation(result_polygon, animation);

			return result_polygon;
		}

		public double Get_Angle()
		{
			if(skelet.Count() > 1)
			{
				double dX = skelet[1].X - skelet[0].X,
					   dY = skelet[1].Y - skelet[0].Y;

				return -Math.Atan2(dX, dY) / Math.PI * 180;
			}
			return 0.0;
		}

        public List<PointD> Copy_Polygon(List<PointD> out_polygon)
		{
			List<PointD> in_polygon = new List<PointD>();
			int num_vertexs = out_polygon.Count();
			for(int V = 0; V < num_vertexs; ++ V)
				in_polygon.Add(new PointD(out_polygon[V]));
			return in_polygon;
		}

		public List<PointD> Get_Bulk_Crosshair(double long_section, double width_beam)
		{
			PointD center = Get_Center();
			List<PointD> result_polygon = Build_Cross(center, long_section, width_beam);

			Animation animation = new Animation();
			animation.turn_priority = 1;
			animation.turn_delta = Get_Angle();
			animation.axis_X_turn = center.X;
			animation.axis_Y_turn = center.Y;

			Polygon_Transformation(result_polygon, animation);

			return result_polygon;
		}

		public List<PointD> Build_Cross(PointD center, double long_section, double width_beam)
		{
            List<PointD> result_polygon = new List<PointD>();

            result_polygon.Add(new PointD(center.X - (long_section + width_beam / 2.0),	center.Y - width_beam / 2.0));
			result_polygon.Add(new PointD(result_polygon[ 0].X + long_section,			result_polygon[ 0].Y));
			result_polygon.Add(new PointD(result_polygon[ 1].X,							result_polygon[ 1].Y - long_section));
			result_polygon.Add(new PointD(result_polygon[ 2].X + width_beam,			result_polygon[ 2].Y));
			result_polygon.Add(new PointD(result_polygon[ 3].X,							result_polygon[ 3].Y + long_section));
			result_polygon.Add(new PointD(result_polygon[ 4].X + long_section,			result_polygon[ 4].Y));
			result_polygon.Add(new PointD(result_polygon[ 5].X,							result_polygon[ 5].Y + width_beam));
			result_polygon.Add(new PointD(result_polygon[ 6].X - long_section,			result_polygon[ 6].Y));
			result_polygon.Add(new PointD(result_polygon[ 7].X,							result_polygon[ 7].Y + long_section));
			result_polygon.Add(new PointD(result_polygon[ 8].X - width_beam,			result_polygon[ 8].Y));
			result_polygon.Add(new PointD(result_polygon[ 9].X,							result_polygon[ 9].Y - long_section));
			result_polygon.Add(new PointD(result_polygon[10].X - long_section,			result_polygon[10].Y));

			return result_polygon;
		}

		public override void Paint_Borders(Graphics g, Pen select_pen, int screen_width, int screen_height)
        {
			if((int)Math.Round(select_pen.Width, MidpointRounding.AwayFromZero) <= 1)
			{
				double long_section = Get_Visual_Radius() - pen.Width / 2.0,
					   width_beam	= BORDER * 2.0 + pen.Width + select_pen.Width;

                MyDraw.Paint_Single_Frame(g, select_pen, screen_height, Get_Bulk_Crosshair(long_section, width_beam), true);
			}
			else
			{
				double long_section = Get_Visual_Radius() - pen.Width / 2.0,
					   width_beam	= BORDER * 2.0 + pen.Width;

                List<List<PointD>> figure = new List<List<PointD>>();
                List<PointD> in_polygon  = Get_Bulk_Crosshair(long_section, width_beam);
                List<PointD> out_polygon = Get_Bulk_Crosshair(long_section, width_beam + select_pen.Width * 2.0);
				figure.Add(in_polygon);
				figure.Add(out_polygon);
				MyDraw.Paint_Bulk_Frame(g, select_pen, screen_height, figure);
			}
		}

		public override bool Is_Include(double X, double Y)
		{
			double long_section = Get_Visual_Radius() - pen.Width / 2.0,
				   width_beam	= BORDER * 2.0 + pen.Width;

			if(Is_Include_Poligon(Get_Bulk_Crosshair(long_section, width_beam), X, Y))
				return true;

			return false;
		}

		public override PointD Get_Center()
		{
			return skelet[0];
		}
	}
}