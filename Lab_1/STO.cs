﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Lab_1
{
    public class STO
    {
        public static List<Broken_Line> Get_Borders(List<Primitive> all_figures, int FC, int FS, bool external)
        {
            List<Broken_Line> broken_lines = new List<Broken_Line>();

            int num_polygons_c = all_figures[FC].multi_figure.Count() - 1,
                num_polygons_s = all_figures[FS].multi_figure.Count() - 1,
                num_vertexs_c  = 0,
                num_vertexs_s  = 0,
                PC   = 0,
                PS   = 0,
                PSW  = 0,
                VC   = 0,
                VS   = 0,
                VSW  = 0,
                VCN  = 0,
                VSN  = 0;

            int  PSP = 0,
                 VSP = 0;

            int first_bl =  0,
                last_bl  = -1;

            List<PointD> polygon_c = new List<PointD>(),
                         polygon_s = new List<PointD>();

            PointD candidate_point    = new PointD(),
                   intersection_point = new PointD(),
                   cur_point          = new PointD();

            double min_dist = 0,
                   new_dist = 0;

            bool start        = false,
                 found        = false,
                 intersection = false,
                 rec          = false,
                 fix          = false;

            for(PC = 0; PC <= num_polygons_c; ++ PC)
            {
                start        = false;
                intersection = false;
                fix          = false;

                polygon_c = all_figures[FC].multi_figure[PC];
                num_vertexs_c = polygon_c.Count() - 1;
                for(VC = 0; VC <= num_vertexs_c; ++ VC)
                {
                    if(!start)
                    {
                        start = true;

                        if( external && !all_figures[FS].Is_Include(polygon_c[VC].X, polygon_c[VC].Y)
                        || !external &&  all_figures[FS].Is_Include(polygon_c[VC].X, polygon_c[VC].Y))
                        {
                            fix = true;
                            broken_lines.Add(new Broken_Line());
                            first_bl = ++ last_bl;
                            rec = true;
                        }
                        else
                            rec = false;
                    }

                    if(rec)     broken_lines[last_bl].Push(polygon_c[VC]);

                    if(VC < num_vertexs_c)      VCN = VC + 1;
                    else                        VCN = 0;

                    cur_point = new PointD(polygon_c[VC].X, polygon_c[VC].Y);

                    PSP = -1;
                    VSP = -1;
                    do
                    {
                        found    = false;
                        min_dist = 0;

                        for(PS = 0; PS <= num_polygons_s; ++ PS)
                        {
                            polygon_s = all_figures[FS].multi_figure[PS];
                            num_vertexs_s = polygon_s.Count() - 1;
                            for(VS = 0; VS <= num_vertexs_s; ++ VS)
                            {
                                if(PSP == PS && VSP == VS)     continue;

                                if(VS < num_vertexs_s)      VSN = VS + 1;
                                else                        VSN = 0;

                                if(is_intersection(cur_point,     polygon_c[VCN],
                                                   polygon_s[VS], polygon_s[VSN], ref candidate_point))
                                {
                                    if(candidate_point.X == cur_point     .X && candidate_point.Y == cur_point.Y)       continue;
                                    if(candidate_point.X == polygon_c[VCN].X && candidate_point.Y == polygon_c[VCN].Y)  continue;

                                    if(candidate_point.X == polygon_s[VS ].X && candidate_point.Y == polygon_s[VS ].Y)  continue;
                                    if(candidate_point.X == polygon_s[VSN].X && candidate_point.Y == polygon_s[VSN].Y)  continue;

                                    new_dist = get_distance(cur_point, candidate_point);

                                    if((new_dist < min_dist) || (!found))
                                    {
                                        found = true;
                                        min_dist = new_dist;

                                        PSW = PS;
                                        VSW = VS;
                                        intersection_point = new PointD(candidate_point.X, candidate_point.Y);
                                    }
                                }
                            }
                        }

                        if(found)
                        {
                            intersection = true;
                            PSP = PSW;
                            VSP = VSW;

                            cur_point = new PointD(intersection_point.X, intersection_point.Y);

                            if(!rec)
                            {
                                broken_lines.Add(new Broken_Line());
                                broken_lines[++ last_bl].Push(intersection_point);
                                broken_lines[   last_bl].Set_In(FC, FS, PC, PSW, VC, VSW);
                                rec = true;
                            }
                            else
                            {
                                rec = false;
                                broken_lines[   last_bl].Set_Out(FC, FS, PC, PSW, VC, VSW);
                            }
                        }
                    }
                    while(found);
                }

                if(fix)
                    if(intersection)
                    {
                        broken_lines[first_bl].in_FC = broken_lines[last_bl].in_FC;
                        broken_lines[first_bl].in_FS = broken_lines[last_bl].in_FS;
                        broken_lines[first_bl].in_PC = broken_lines[last_bl].in_PC;
                        broken_lines[first_bl].in_PS = broken_lines[last_bl].in_PS;
                        broken_lines[first_bl].in_VC = broken_lines[last_bl].in_VC;
                        broken_lines[first_bl].in_VS = broken_lines[last_bl].in_VS;

                        broken_lines[first_bl].broken_line.InsertRange(0, broken_lines[last_bl].broken_line);
                        broken_lines.RemoveAt(last_bl --);
                    }
                    else
                        broken_lines[last_bl].set_figure();
            }

            return broken_lines;
        }

        public static List<List<PointD>> collector(List<Broken_Line> broken_lines)
        {
            List<List<PointD>> sort_figure      = new List<List<PointD>>();
            List<bool>         done_broken_line = new List<bool>();
            List<PointD>       temp_polygon     = new List<PointD>();
            int num_broken_lines = 0,
                counter_done_broken_lines = 0,
                BLB = 0,
                BLS = 0,
                BLN = 0,
                begin = 0;
            bool begin_found = false;

            num_broken_lines = broken_lines.Count();
            for(BLN = 0; BLN < num_broken_lines; ++ BLN)
                if(broken_lines[BLN].is_figure())
                {
                    ++ counter_done_broken_lines;
                    done_broken_line.Add(true);
                    Primitive.Push_Polygon(ref sort_figure, broken_lines[BLN].broken_line);
                }
                else    done_broken_line.Add(false);

            bool progress = true;
            while(counter_done_broken_lines < num_broken_lines)
            {
                progress = false;
                for(BLS = begin; BLS < num_broken_lines; ++ BLS)
                    if(!done_broken_line[BLS])
                        if(!begin_found)
                        {
                            progress = true;
                            ++ counter_done_broken_lines;

                            done_broken_line[BLS] = true;
                            BLB = BLN = begin = BLS;
                            temp_polygon.Clear();
                            temp_polygon.AddRange(broken_lines[BLN].broken_line);
                            begin_found = true;
                        }
                        else
                            if((broken_lines[BLN].out_FC == broken_lines[BLS].in_FS)  &&
                               (broken_lines[BLN].out_FS == broken_lines[BLS].in_FC)  &&
                               (broken_lines[BLN].out_PC == broken_lines[BLS].in_PS)  &&
                               (broken_lines[BLN].out_PS == broken_lines[BLS].in_PC)  &&
                               (broken_lines[BLN].out_VC == broken_lines[BLS].in_VS)  &&
                               (broken_lines[BLN].out_VS == broken_lines[BLS].in_VC))
                            {
                                progress = true;
                                ++ counter_done_broken_lines;

                                done_broken_line[BLS] = true;
                                temp_polygon.AddRange(broken_lines[BLS].broken_line);
                                if((broken_lines[BLS].out_FC == broken_lines[BLB].in_FS)  &&
                                   (broken_lines[BLS].out_FS == broken_lines[BLB].in_FC)  &&
                                   (broken_lines[BLS].out_PC == broken_lines[BLB].in_PS)  &&
                                   (broken_lines[BLS].out_PS == broken_lines[BLB].in_PC)  &&
                                   (broken_lines[BLS].out_VC == broken_lines[BLB].in_VS)  &&
                                   (broken_lines[BLS].out_VS == broken_lines[BLB].in_VC))
                                {
                                    Primitive.Push_Polygon(ref sort_figure, temp_polygon);
                                    begin_found = false;
                                    break;
                                }
                                else    BLN = BLS;
                            }
                if( ! progress)
                    throw new InvalidOperationException("Бесконечный цикл!");
            }
            return sort_figure;
        }

        public static double get_distance(Point A, Point B)
        {
            return get_distance(new PointD(A), new PointD(B));
        }
        
        public static double get_distance(PointD A, PointD B)
        {
            double k1 = A.X - B.X;
            double k2 = A.Y - B.Y;
            return Math.Sqrt(k1 * k1 + k2 * k2);
        }
        
        public static bool is_intersection(PointD A1, PointD A2, PointD B1, PointD B2, ref PointD R)
        {
            double denom  = ((B2.Y * A2.X - B2.Y * A1.X) - (B1.Y * A2.X - B1.Y * A1.X)) -
                            ((B2.X * A2.Y - B2.X * A1.Y) - (B1.X * A2.Y - B1.X * A1.Y));

            double nume_a = ((B2.X * A1.Y - B2.X * B1.Y) - (B1.X * A1.Y - B1.X * B1.Y)) -
                            ((B2.Y * A1.X - B2.Y * B1.X) - (B1.Y * A1.X - B1.Y * B1.X));

            double nume_b = ((A2.X * A1.Y - A2.X * B1.Y) - (A1.X * A1.Y - A1.X * B1.Y)) -
                            ((A2.Y * A1.X - A2.Y * B1.X) - (A1.Y * A1.X - A1.Y * B1.X));

            if(Math.Abs(denom) <= 0.000001)
            {
                if (Math.Abs(nume_a) <= 0.000001 && Math.Abs(nume_b) <= 0.000001)
                {
                    // return COINCIDENT;
                    throw new InvalidOperationException("Проверка пересечения отрезков: отрезки совпадают!");
                }

                // return PARALLEL;
                // throw new InvalidOperationException("Проверка пересечения отрезков: отрезки параллельны!");
                return false;
            }

            double ua = nume_a / denom;
            double ub = nume_b / denom;
 
            if (ua > -0.000001 && ua < 1.000001 && ub > -0.000001 && ub < 1.000001)
            {
                // Получаем точку пересечения.
                R.X = A1.X + ua * A2.X - ua * A1.X;
                R.Y = A1.Y + ua * A2.Y - ua * A1.Y;
                
                return true; // INTERESECTING;
            }
            return false; // NOT_INTERESECTING;
        }

        public static bool clockwise_direction(List<Point> polygon) {
            return clockwise_direction(Primitive.Polygon_Int_to_Double(polygon));
        }

        public static bool clockwise_direction(List<PointD> polygon)
        {
            int num_vertex = polygon.Count() - 1;
            int C = 0;
            double Y_min = polygon[0].Y;

            for(int v = 1; v <= num_vertex; ++ v)
                if(polygon[v].Y < Y_min)
                {
                    Y_min = polygon[v].Y;
                    C     = v;
                }

            int P = 0,
                N = 0;
            if(C == 0)              P = num_vertex;
            else                    P = C - 1;
            if(C == num_vertex)     N = 0;
            else                    N = C + 1;
            if(((polygon[P].X - polygon[N].X) * (polygon[C].Y - polygon[N].Y) - (polygon[C].X - polygon[N].X) * (polygon[P].Y - polygon[N].Y)) / 2 < 0)
                return false;
            return true;
        }
    }
}