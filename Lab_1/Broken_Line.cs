﻿using System;
using System.Collections.Generic;

namespace Lab_1 {
    [Serializable]
    public class Broken_Line
    {
        public List<PointD> broken_line = new List<PointD>();
        public List<int> vertexs_id = new List<int>();

        private bool figure = false;

        public int in_FC  = 0,
                   in_FS  = 0,
                   in_PC  = 0,
                   in_PS  = 0,
                   in_VC  = 0,
                   in_VS  = 0;

        public int out_FC = 0,
                   out_FS = 0,
                   out_PC = 0,
                   out_PS = 0,
                   out_VC = 0,
                   out_VS = 0;

        public Broken_Line() {}

        public bool is_figure() {
            return figure;
        }

        public void set_figure() {
            figure = true;
        }
        
        public void Push(PointD V)
        {
            PointD temp = new PointD(V.X, V.Y);
            broken_line.Add(temp);
        }
    
        public void Set_In(int FC, int FS, int PC, int PS, int VC, int VS)
        {
            in_FC  = FC;
            in_FS  = FS;
            in_PC  = PC;
            in_PS  = PS;
            in_VC  = VC;
            in_VS  = VS;
        }

        public void Set_Out(int FC, int FS, int PC, int PS, int VC, int VS)
        {
            out_FC  = FC;
            out_FS  = FS;
            out_PC  = PC;
            out_PS  = PS;
            out_VC  = VC;
            out_VS  = VS;
        }
    }
}